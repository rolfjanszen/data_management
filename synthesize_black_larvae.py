import numpy as np
from collections import namedtuple
import cv2
from os import listdir, path, mkdir
from os.path import isfile, join, expanduser, isdir
import pickle as pkl


def get_images(file_path, format='.jpg'):

    if not isdir(file_path):
        return []

    file_list = [join(file_path, f) for f in listdir(file_path)
                 if isfile(join(file_path, f)) and f[-4:] == format]
    file_list.sort()
    return file_list


def get_image_pairs(files, name_add1, name_add2, format1='.png', format2='.png'):

    pair_obj = namedtuple('im_pair', [name_add1, name_add2])
    image_pairs = []

    len_name_end1 = len(name_add1 + format1)
    len_name_end2 = len(name_add2 + format2)

    while len(files):

        file1 = files[0]
        files.pop(0)
        found_other_file = False
        if name_add1 + format1 in file1:
            len_name_end = len_name_end2
            name_file = file1[:-len_name_end1]
        else:
            len_name_end = len_name_end1
            name_file = file1[:-len_name_end2]

        for i, file in enumerate(files):
            if file[:-len_name_end] == name_file:
                file2 = file
                files.pop(i)
                found_other_file = True
                break

        if found_other_file:
            if file1[-len_name_end:] == name_add1 + format1:
                new_pair = pair_obj(file2, file1)
            else:
                new_pair = pair_obj(file1, file2)

            image_pairs.append(new_pair)
        else:
            print('could not find complete', name_file)

    return image_pairs


class Backgrounds:
    images = []

    def __init__(self, path):
        self.images = get_images(path, '.png')
        if len(self.images) < 1:
            print("ERROR: no background images found in ",path)
            exit(0)
            
    def get_patch(self, width, heigth):
        
        im_name = np.random.choice(self.images)
        im = cv2.imread(im_name, 0)

        w, h = im.shape[:2]
        if w - width > 1:
            w_coord = np.random.randint(0, w - width)
        else:
            w_coord = 0
        if h - heigth > 1:
            h_coord = np.random.randint(0, h - heigth)
        else:
            h_coord = 0

        print('get_patch w_coord ', w_coord, 'width ',
              width, 'h_coord', h_coord, 'heigth', heigth)
        if im.shape[0] < width or im.shape[1] < heigth:
            print('ERRROR: image with too small dimension: ', im_name)
            return [], False
        im_crop = im[w_coord:(w_coord + width), h_coord:(h_coord + heigth)]
        print('im_crop', im_crop.shape, ' im shp ', im.shape)
        rand_entry = np.random.randint(-1, 3)
        im_flipped = cv2.flip(im_crop, rand_entry)
        return im_flipped, True


class LarveaGenerator:

    larvae = []

    def __init__(self, path):
        inp_files = get_images(path, '.png')
        
        self.files = inp_files
        self.larvae = get_image_pairs(
            self.files, name_add1='mask', name_add2='org')
        if len(self.larvae) < 1:
            print("ERROR: no larvae images found in ",path)
            exit(0)

#         self.create_larvae_list()

    def create_larvae_mask(self, height, width, nr_larvae):

        mask = np.zeros([height, width], dtype=np.float32)
        occupancy_map = np.zeros([height, width], dtype=np.uint8)
        larvae_set = np.zeros([height, width], dtype=np.float32)

        rand_larv_nr = np.random.randint(
            int(nr_larvae / 2), nr_larvae + 1, 1)[0]
        print('rand_larv_nr', rand_larv_nr)
        augmented_larvae = self.get_larvae(rand_larv_nr)
        mid_points = []
        pad = 32
        max_dimension = height - 2.1 * pad

        for larva in augmented_larvae:
            w, h = larva['im'].shape[:2]

            if w > (width - 2.2 * pad) or h > (height - 2.2 * pad):
                #                 print(' width ',width,' w ',w)
                re_scale = (width - 2.2 * pad) / w
                re_scaling_h = (height - 2.2 * pad) / h
                if re_scaling_h < re_scale:
                    re_scale = re_scaling_h

                larva['im'] = cv2.resize(
                    larva['im'], (0, 0), fx=re_scale, fy=re_scale)
                larva['mask'] = cv2.resize(
                    larva['mask'], (0, 0), fx=re_scale, fy=re_scale)
                w, h = larva['im'].shape[:2]

#                 print(
#                     'requested dimensions too small. Increase req. width, height. mask: w,h', w, h)

            w_coord = np.random.randint(pad, width - w - pad)
            h_coord = np.random.randint(pad, height - h - pad)
            new_mask = np.zeros([height, width], dtype=np.uint8)
            new_mask[w_coord:w_coord + w,
                     h_coord:h_coord + h] = larva['mask'] / 255

            occupancy_sum = new_mask + mask
            crossed_occupancy = np.column_stack(np.where(occupancy_sum > 1.5))

            if len(crossed_occupancy) < 10:
                # coord_range = range(w_coord:w_coord + w, h_coord:h_coord + h)

                mask[w_coord:w_coord + w, h_coord:h_coord +
                     h] += larva['mask'] / 255
                masked_image = larva['im'].astype(
                    np.float32) * (larva['mask'] / 255)
                larvae_set[w_coord:w_coord + w,
                           h_coord:h_coord + h] += masked_image
                mid_point = [int(w_coord + (w / 2)), int(h_coord + (h / 2))]
                mid_points.append(mid_point)
                print('fine')
            else:
                print('clash')

        if len(mid_points) < 1:
            print('no bugs in pic')

        mask = mask.astype(np.float)
        larvae_set = larvae_set.astype(np.float) * mask
#         for pnt in mid_points:
#             cv2.circle(larvae_set,(pnt[1],pnt[0]), 10, (100,155,0), 2)
#         cv2.imshow('larvae_set',larvae_set)
#         cv2.waitKey()

        point_map = self.create_point_map(mid_points, height, width)
#         cv2.imshow('rot_im', larvae_set.astype(np.uint8))
#         cv2.imshow('point_map', (point_map * 255).astype(np.uint8))
#         cv2.waitKey()
        return larvae_set, point_map, mask

    def create_point_map(self, mid_points, height, width):
        hlf_K = 64
        point_map = np.zeros([height, width], dtype=np.float32)
        gauss_kernel = cv2.getGaussianKernel(2 * hlf_K, 30)
        gauss_2d = gauss_kernel * np.transpose(gauss_kernel)
        normfactor = 1 / np.max(gauss_2d)
        for point in mid_points:
            point_map[(point[0] - hlf_K):(point[0] + hlf_K), (point[1] -
                                                              hlf_K):(point[1] + hlf_K)] += gauss_2d * normfactor

        point_map[np.where(point_map > 0.99)] = 1.0

        return point_map

    def get_larvae(self, amount=1):

        augmented_larvae = []
        nr_larva = len(self.larvae)
    
        for i in range(amount):

            x_scale = np.random.uniform(0.7, 1.0, 1)
            y_scale = np.random.uniform(0.7, 1.0, 1)

            bright = np.random.uniform(1.2, 1.6, 1)

            entry = np.random.randint(nr_larva)
            larva = self.larvae[entry]
            angle = np.random.randint(0, 360)

            im = cv2.imread(larva.org, 0)

            mask = cv2.imread(larva.mask, 0)
            rot_im = self.rotate(im, angle)
            mask_im = self.rotate(mask, angle)
            w,h = rot_im.shape[:2]
            noise = np.random.uniform(-5,5, [w,h])
            rot_im = (rot_im+noise)*bright
            
            mask_im = cv2.resize(mask_im, (0, 0), fx=x_scale, fy=y_scale)
            rot_im = cv2.resize(rot_im.astype(np.uint8), (0, 0), fx=x_scale, fy=y_scale)

            augmented_larvae.append({'im': rot_im, 'mask': mask_im})

        return augmented_larvae

    def rotate(self, image, angleInDegrees):
        h, w = image.shape[:2]
        img_c = (w / 2, h / 2)

        rot = cv2.getRotationMatrix2D(img_c, angleInDegrees, 1)

        rad = np.deg2rad(angleInDegrees)
        s = np.sin(rad)
        c = np.cos(rad)
        b_w = int((h * abs(s)) + (w * abs(c)))
        b_h = int((h * abs(c)) + (w * abs(s)))

        rot[0, 2] += ((b_w / 2) - img_c[0])
        rot[1, 2] += ((b_h / 2) - img_c[1])

        outImg = cv2.warpAffine(image, rot, (b_w, b_h), flags=cv2.INTER_LINEAR)
        return outImg


class BLSynthesiser:

    base_folder = expanduser('~/Labeled')
    inp_name_end = 'inp'
    outp_name_end = 'outp'
    save_format = '.png'
    save_format_outp = '.bmp'
    im_pairs = []
    mean = 120
    std = 66

    def __init__(self, larvae_path, back_grnd_path, store_folder, nr_larvae):
        self.back = Backgrounds(back_grnd_path)
        self.larva_gen = LarveaGenerator(larvae_path)
        self.nr_larvae = nr_larvae
        if not nr_larvae:
            print("ERROR!!!! NO LARVAE IMAGES WHERE PROVIDED FOR SYNTHESIZE")
            exit(0)
        self.save_folder = join(self.base_folder, store_folder)
        if not isdir(self.save_folder):
            mkdir(self.save_folder)

    def normalize(self, image):
        image_norm = (image.astype(np.float) - self.mean) / self.std
        return image_norm

    def syntesize_in_out(self, width, heigth, scale, down_sample):

        back_ground, ret = self.back.get_patch(width, heigth)

        if not ret:
            return [], [], ret
        larvae_set, point_labels, mask = self.larva_gen.create_larvae_mask(
            heigth, width, self.nr_larvae)
        print('back_ground shp', back_ground.shape, 'mask shp ', mask.shape)
        back_ground = back_ground * (np.ones_like(mask) - mask)

        back_ground = back_ground + larvae_set

        input = cv2.resize(back_ground, (0, 0), fx=scale, fy=scale)
        output = cv2.resize(point_labels, (0, 0), fx=scale /
                            down_sample, fy=scale / down_sample)

        w, h = input.shape
        input = np.reshape(input, [w, h, 1])
        w, h = output.shape

        output = np.reshape(output, [w, h, 1])
#         cv2.imshow('input',input.astype(np.uint8))
#         cv2.waitKey()

        return input, output, True

    def create_in_output(self, batch, width, heigth, scale, down_sample):
        #         inputs = []
        #         outputs = []

        nr_images_stored = self.create_base_path(width, scale)

        input_append = '_' + self.inp_name_end + self.save_format
        output_append = '_' + self.outp_name_end + '.bmp'
        for i in range(batch):
            input, output, ret = self.syntesize_in_out(
                width, heigth, scale, down_sample)

            if ret:
                save_name = join(self.full_base_path,
                                 str(nr_images_stored + i))
                input_name = save_name + input_append
                output_name = save_name + output_append
                print('created ', input_name)
#                 cv2.imshow('input',input.astype(np.uint8))
#                 cv2.waitKey()
                cv2.imwrite(input_name, input.astype(np.uint8))
                cv2.imwrite(output_name, output)

#         return np.array(inputs), np.array(outputs)

    def create_base_path(self, width, scale):

        self.full_base_path = join(
            self.save_folder, str(width) + '_' + str(scale))
        if not isdir(self.full_base_path):
            mkdir(self.full_base_path)

        list_images = get_images(self.full_base_path, self.save_format)
        nr_images = len(list_images)
#         self.full_base_path = join(self.save_folder, folder_name)

        return nr_images

    def retrieve_stored_batch(self, batch_size, width, scale):

        if len(self.im_pairs) < 1:
            self.start_batch = 0
            self.create_base_path(width, scale)
            inp_file_names = get_images(self.full_base_path, self.save_format)
            outp_file_names = get_images(
                self.full_base_path, self.save_format_outp)
            file_names = inp_file_names + outp_file_names
            file_names.sort()
            self.im_pairs = get_image_pairs(
                file_names, self.inp_name_end, self.outp_name_end, self.save_format, '.bmp')
            np.random.shuffle(self.im_pairs)

        if len(self.im_pairs) < 1:
            print('no in- output files found. Please create these first')
            return [], []

        if self.start_batch + batch_size > len(self.im_pairs):
            self.start_batch = 0

        images_to_load = self.im_pairs[self.start_batch:self.start_batch + batch_size]
        inputs = []
        outputs = []
        originals = []
        for im_load in images_to_load:

            input = im_load.inp
            output = im_load.outp
            inp_im = cv2.imread(input, 0)
            originals.append(inp_im)

            outp_im = cv2.imread(output, 0)
            inp_im = np.expand_dims(inp_im, axis=2)
            inp_im_norm = self.normalize(inp_im)
            outp_im = np.expand_dims(outp_im, axis=2)
            inputs.append(inp_im_norm)
            outputs.append(outp_im)

        self.start_batch += batch_size

        return np.array(inputs, dtype=np.float32), np.array(outputs, dtype=np.float32), originals


def test():

    bug_mask = expanduser('~/Data/bug_masks')
    background_images = expanduser('~/Data/videos_box2_3/black_larveatest_img')

    # bug_mask = expanduser('~/Data/record_data_box2.0/bug_masks')
    # background_images = expanduser('~/Data/record_data_box2_3/black_larveatest_img')

    store_folder = 'test_points'
    gen = BLSynthesiser(bug_mask, background_images, store_folder, 16)
    re_scale = 0.25
    gen.create_in_output(100, 256, 256, re_scale, 8)
    inp, out, org = gen.retrieve_stored_batch(1, 512, re_scale)
    cv2.imshow('inp', org[0])
    cv2.waitKey()


if __name__ == '__main__':
    test()
