from  os.path import join, expanduser, isdir
from sys import path
from os import makedirs
import paramiko
import getpass


path.append(expanduser('~/scripts/tools'))
from helpers import timestamp_from_data_time

def get_folders_ls(stdout):
    folders = []
    for f in stdout.readlines():
        folders.append(f.split()[-1])
    return folders


paramiko.util.log_to_file("paramiko.log")

# Open a transport#
ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
pswd = getpass.getpass('Password:')
ssh_client.connect(hostname='192.168.0.3',username='container',password=pswd)
# Au
start_date = '2020-06-18_02_00_00'

search_folder = '/media/container/SWAPDISK1'
stdin, stdout, stderr =ssh_client.exec_command('ls '+search_folder)
folders = get_folders_ls(stdout)
sub_folder = 'recordings'
file_list = []
str_date_key = "%Y-%m-%d_%H_%M_%S"
start_t_stampe = timestamp_from_data_time(start_date, str_date_key)

for folder in folders:
    stdin, stdout, stderr = ssh_client.exec_command('ls ' + join(search_folder, folder, sub_folder))
    files = get_folders_ls(stdout)
    print('checking', folder)  #
    for f in files:
        t_stamp = timestamp_from_data_time(f[:-4], str_date_key)
        if t_stamp > start_t_stampe:
            full_path = join(search_folder, folder, sub_folder, f)
            new_file = {'path': full_path, 'traynr': folder[-5:], 'name': f}
            file_list.append(new_file)

print('getting files',len(file_list))
ftp_client=ssh_client.open_sftp()
localfilepath = expanduser('~/Data/woodfarm/recordings')
for i, f in enumerate(file_list[45:]):
    save_path = join(localfilepath,f['traynr']+f['name'])
    ftp_client.get(f['path'],save_path)
    print('nr: ',i,'got ',f['path'])
# ftp_client.close()
