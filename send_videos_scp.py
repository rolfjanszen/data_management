import argparse
from os import makedirs
from os.path import expanduser
from os.path import isdir, join
from shutil import copyfile
from sys import argv
import cv2
import numpy as np
import paramiko

from get_meas_downscaled import get_file_from_date


# def get_sequences(file_path, from_timestamp=None):
#     file_lst = []
#     file_folders = listdir(file_path)
#     for folder in file_folders:
#         rec_path = join(file_path, folder, 'recordings')
#         if isdir(rec_path):
#             vid_list = get_file_list(rec_path, 'avi')
#             full_vid_list = [join(folder, 'recordings', f) for f in vid_list]
#             if from_timestamp is not None:
#                 full_vid_list = seperate_from_date(full_vid_list, from_timestamp)
#
#             if len(full_vid_list):
#                 file_lst = file_lst + full_vid_list
#     np.random.shuffle(file_lst)
#     if len(file_lst) < 1:
#         file_lst = get_file_list(file_path, 'avi')
#
#     return file_lst

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--file_path', help="path to where tray folders are stored",
                        type=str, required=False, default='/media/x1-2/SWAPDISK')
    parser.add_argument('-f', '--format', help="Data format to save files in",
                        type=str, required=False, default='avi')
    parser.add_argument('-d', '--start_date', help="Date from when to get files. Format example 2021-07-27_01_00_00",
                        type=str, required=False, default='2021-07-27_01_00_00')
    parser.add_argument('-s', '--send_folder', help="folder receiving end",
                        type=str, required=False, default='/home/rolf/x1-2-recordings')
    parser.add_argument('-m', '--max_files', help="max number of files to send",
                        type=int, required=False, default=9)

    args = parser.parse_args(argv[1:])
    search_folder = expanduser(args.file_path)
    start_date = args.start_date
    data_format = args.format
    max_nr_files = args.max_files

    server = '10.187.209.207'
    user = 'rolf'
    save_folder = expanduser('~/temp_store')
    if not isdir(save_folder):
        makedirs(save_folder)
    file_info_list = get_file_from_date(search_folder, data_format, start_date)
    print("found", len(file_info_list),"files")
    if len(file_info_list) > max_nr_files:
        file_info_list = np.random.choice(file_info_list, max_nr_files)
    # password = getpass()
    # ssh = createSSHClient(server, 22, user, password)
    # scp = SCPClient(ssh.get_transport())

    for file_info in file_info_list:
        full_file_path = file_info['path']

        # scp.put(full_file_path, args.send_folder)

        #       if int(file_info['traynr']) > 400:
        #            continue

        save_name = file_info['traynr'] + '_' + file_info['name']
        # destination_path = join(args.send_folder,save_name)
        # scp.put(full_file_path, destination_path)
        destination_path = join(save_folder,save_name)
        copyfile(full_file_path, destination_path)
