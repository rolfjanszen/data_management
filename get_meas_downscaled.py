import cv2
from  os.path import join, expanduser, isdir

from os import listdir, makedirs
from sys import argv
import argparse
import shutil
import numpy as np
from tools.general.helpers import timestamp_from_data_time, get_file_list


def get_file_from_date(search_folder,data_format,start_date):

    str_date_key = "%Y-%m-%d_%H_%M_%S"
    sub_folder = 'recordings'
    start_t = timestamp_from_data_time(start_date, str_date_key)
    folder_list = [join(search_folder, d) for d in listdir(search_folder) if
                   isdir(join(search_folder, d, sub_folder))]

    file_list = []

    for d in folder_list:
        meas_dir = join(d, sub_folder)
        if isdir(meas_dir):
            files = get_file_list(meas_dir,data_format)
            for f in files[::-1]:
                f_stamp = f[:-4]
                file_t = timestamp_from_data_time(f_stamp,str_date_key)
                if start_t >= file_t:
                    break
                full_path = join(meas_dir,f)
                new_file = {'path': full_path, 'traynr': d[-5:-2], 'name': f}
                file_list.append(new_file)
    return file_list


def get_scaled_images(file_list, data_format,save_folder):
    for file in file_list:
        if data_format == 'avi':
            cap = cv2.VideoCapture(file['path'])
            ret, im = cap.read()
        else:
            im = cv2.imread(file['path'])
        im = cv2.resize(im, (0, 0), fx=0.5, fy=0.5)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        save_name = file['traynr'] + '_' + file['name']
        new_save_loc = join(save_folder, save_name)
        cv2.imwrite(new_save_loc, im)


def get_scaled_sequence(file_list, data_format,save_folder):

    if data_format != 'avi':
        print("ERROR: Cannot create sequence from single image")
        return

    len_file_list = len(file_list)
    for i, file in enumerate(file_list):
        print('processing file:',file,' ',i,'/', len_file_list)
        cap = cv2.VideoCapture(file['path'])
        new_sequence = []
        for i in range(3):
            ret, im = cap.read()
            im = cv2.resize(im, (0, 0), fx=0.25, fy=0.25)
            im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            new_sequence.append(im)
            cap.set(cv2.CAP_PROP_POS_FRAMES,i * 5)

        save_name = file['traynr'] + '_' + file['name']
        new_save_loc = join(save_folder, save_name)
        # cv2.imwrite(new_save_loc, im)
        np.savez_compressed(new_save_loc,np.array(new_sequence))



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--file_path', help="path to where tray folders are stored",
                        type=str, required=False,default='/media/container/SWAPDISK2')
    parser.add_argument('-f', '--format', help="Data format to save files in",
                        type=str, required=False, default='avi')
    parser.add_argument('-d', '--start_date', help="Date from when to get files",
                        type=str, required=False, default='2021-04-18_01_00_00')

    args = parser.parse_args(argv[1:])
    search_folder = args.file_path
    start_date = args.start_date
    data_format = args.format
    save_folder = expanduser('~/temp_store')
    save_path = join(search_folder, save_folder)
    file_list = get_file_from_date(search_folder,data_format,start_date)

    if not isdir(save_path):
        makedirs(save_path)
    print("found ",len(file_list)," files")

    # get_scaled_images(file_list, data_format,save_folder)
    get_scaled_sequence(file_list, data_format, save_path)
    # with py7zr.SevenZipFile('images.7z', 'w') as archive:
    #     archive.writeall(save_folder, 'base')
    shutil.make_archive(expanduser('~/temp_Store.zip'), 'zip', save_path,save_folder)


