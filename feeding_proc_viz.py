import datetime
import sys
import time
from os import makedirs, listdir
from os.path import join, expanduser, isdir
import json
import cv2
import numpy as np
import pandas as pd
from math import isnan
tool_box_path = expanduser('~/scripts/tools')
sys.path.append(tool_box_path)
import matplotlib.pyplot as plt
from helpers import get_file_list, to_time_stamp_list, find_nearest, create_overlay, thermal_to_rgb

BASE_PATH = expanduser('~/')

class FeedProcessVisualizer:
    max_t_diff = 200  # seconds
    max_width = 1000
    out_vid = None
    overlay_vid = None
    frame_rate = 8

    thermal_files = []

    def __init__(self, box_path, csv_name, feed_map_sub_dir, ir_image_sub_dir,files_to_skip, use_npz = True):

        self.make_save_path(box_path)
        self.use_npz_therm = use_npz

        if not isdir(self.save_path):
            makedirs(self.save_path)
        self.csv_path = join(box_path, csv_name)
        self.feed_map_path = join(box_path, feed_map_sub_dir)
        self.ir_image_path = join(box_path, ir_image_sub_dir)

        self.thermal_path = join(box_path, 'thermal_grid')

        self.bl_im_path = join(box_path, 'bl_count')

        self.figure = plt.figure(figsize=(25,15 ))
        self.files_to_skip = files_to_skip

    def make_save_path(self, data_path):
        base_path_len = len(BASE_PATH)
        if data_path[:base_path_len] is not BASE_PATH:
            folders = data_path.split('/')
            data_loc = [i for i, name in enumerate(folders) if name == 'Data']
            if len(data_loc):
                new_path = join(*folders[data_loc[0]:])
                data_path = join(BASE_PATH,new_path)
            else:
                data_path = BASE_PATH
        self.save_path = join(data_path, 'plots')

    def get_thermal_image(self, row, t_stamp):

        if self.use_npz_therm:
            idx, diff_feed = find_nearest(self.thermal_tstamp, t_stamp)
            if diff_feed > 7200:
                return np.array([[[0] * 3] * 32] * 24, dtype=np.uint8)
            file = join(self.thermal_path,self.thermal_files[idx])
            therm_array = np.load(file)
            therm_array= therm_array['arr_0']
            print('therm_array',therm_array.shape)
            len_arr = therm_array.shape[0]
            thermal_extent = np.array([0]*(768 - len_arr))
            therm_array=np.concatenate((therm_array, thermal_extent), axis=0)
            if therm_array.shape[0] != 768:
                return np.array([[[0] *3]* 32] * 24,dtype=np.uint8)
            therm_matrix = np.reshape(therm_array,[24, 32])
            bb_coord = [[9, 0], [23, 25]]
            therm_matrix = therm_matrix[bb_coord[0][1]:bb_coord[1][1], bb_coord[0][0]:bb_coord[1][0]]

            thermal_im= thermal_to_rgb(therm_matrix,24,35, (384,512))
        else:
            try:
                thermal_str = row['Thermal grid (C)']
                thermal_pixels = json.loads(thermal_str)
                thermal_im = np.reshape(thermal_pixels, [8, 8])
            except:
                thermal_im = np.array([[[0] *3]* 8] * 8,dtype=np.uint8)
        return thermal_im

    def get_corresponding_data(self):

        df = pd.read_csv(self.csv_path)
        feed_maps = get_file_list(self.feed_map_path, 'hm_rslt.png')
        feed_maps_overlay = get_file_list(self.feed_map_path, 'hm_overlay.png')
        ir_images = get_file_list(self.ir_image_path, 'jpg')
        blac_larvae = get_file_list(self.bl_im_path, '.jpg')
        blac_larvae = blac_larvae[::3]
        segmentation_image =get_file_list(self.feed_map_path, 'food_larv_overlay.png')
        feed_maps=feed_maps[self.files_to_skip:]
        ir_images=ir_images[self.files_to_skip:]

        # if self.use_npz_therm:
        #     self.thermal_files =  get_file_list(self.thermal_path, '.npz')
        #     self.thermal_tstamp = to_time_stamp_list(self.thermal_files)

        feed_maps_overlay = feed_maps_overlay[self.files_to_skip:]
        blac_larvae = blac_larvae[self.files_to_skip:]
        segmentation_image = segmentation_image[self.files_to_skip:]

        feedmap_tstamp = to_time_stamp_list(feed_maps)
        images_tstamp = to_time_stamp_list(ir_images)
        blac_larvae_tstamp = to_time_stamp_list(blac_larvae)
        feed_frac_list = []
        larvae_segment = []
        food_segmented = []
        for indx, row in df.iloc[self.files_to_skip:].iterrows():
            date = row['Time and date']
            feed_frac = row['Feed fraction']
            larvae = row['Larvae segment avg']
            food_segment = row['Feed segment avg']
            larvae_segment.append(larvae)
            food_segmented.append(food_segment)
            feed_frac_list.append(feed_frac)

            print('indx',indx)
            t_stamp = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").timetuple())
            idx_feed, diff_feed = find_nearest(feedmap_tstamp, t_stamp)
            idx_image, diff_image = find_nearest(images_tstamp, t_stamp)

            # thermal_im= self.get_thermal_image(row, t_stamp)
            thermal_im=[]

            # thermal_im = cv2.imread(join(self.feed_map_path, segmentation_image[idx_feed]))
            if idx_feed >= len(blac_larvae):
                bl_image = cv2.imread(join(self.bl_im_path, blac_larvae[0]))
            else:
                bl_image = cv2.imread(join(self.bl_im_path, blac_larvae[idx_feed]))


            if diff_feed < self.max_t_diff and diff_image < self.max_t_diff:

                comb_image, overlay_image = self.combine_data(ir_images[idx_image], feed_maps[idx_feed], feed_maps_overlay[idx_feed],feed_frac_list, larvae_segment,food_segmented, date, thermal_im,bl_image, segmentation_image[idx_feed])
                comb_image = cv2.resize(comb_image,(0,0),fx=0.5,fy=0.5)
                cv2.imshow('comb_image',comb_image)
                cv2.waitKey(20)

                if self.out_vid == None:
                    w, h = comb_image.shape[:2]
                    w_overlay, h_overlay = overlay_image.shape[:2]
                    vid_name_path = join(self.save_path, 'Feeding_process.avi')
                    overlay_vid_name_path = join(self.save_path, 'Feeding_overlay.avi')
                    fourcc = cv2.VideoWriter_fourcc(*'XVID')
                    self.out_vid = cv2.VideoWriter(vid_name_path, fourcc, self.frame_rate, (h, w))
                    self.overlay_vid= cv2.VideoWriter(overlay_vid_name_path, fourcc, self.frame_rate, (h_overlay, w_overlay))
                self.out_vid.write(comb_image)
                self.overlay_vid.write(overlay_image)
            else:
                print("couldn't find pair for ",date)

        files = get_file_list(self.save_path,'png')
        for file in files:

            if self.out_vid == None:
                w, h = file.shape[:2]
                vid_name_path = join(self.save_path, 'Feeding_process.avi')
                fourcc = cv2.VideoWriter_fourcc(*'XVID')
                self.out_vid = cv2.VideoWriter(vid_name_path, fourcc, self.frame_rate, (h, w))

            self.out_vid.write(file)

        self.overlay_vid.release()
        self.out_vid.release()

    def combine_data(self, image_name, feed_map_name, feed_map_overlay, feed_frac_list, larvae_segment,food_segmented, name, thermal_im, black_larvae,segmentation_image):

        print('processing ', name)
        image = cv2.imread(join(self.ir_image_path, image_name))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        feed_map = cv2.imread(join(self.feed_map_path, feed_map_name), 0)
        overlay = cv2.imread(join(self.feed_map_path, feed_map_overlay), 0)
        segmentation_overlay = cv2.imread(join(self.feed_map_path, segmentation_image))

        # overlay = cv2.cvtColor(overlay, cv2.COLOR_BGR2RGB)
        # overlay = create_overlay(feed_map, image)
        # thermal_map = thermal_to_rgb(thermal_im, 24, 33)
        thermal_map = cv2.cvtColor(thermal_im, cv2.COLOR_BGR2RGB)
        plt.rcParams.update({'font.size': 15})
        self.figure.clf()
        round_frac = np.around(feed_frac_list[-1],decimals=3)
        figure_name = 'mean: '+str(round_frac) + '       ' + name
        self.figure.suptitle(figure_name)
        plot = self.figure.add_subplot(2, 3, 1)
        plot.set_title('Thermal imaging \n')
        plot.imshow(thermal_map)
        plot.axis('off')

        plot = self.figure.add_subplot(2, 3, 2)
        plot.imshow(image)
        plot.set_title('Visible light image \n')
        plot.axis('off')

        plot = self.figure.add_subplot(2, 3, 3)
        plot.plot(larvae_segment,'g')
        plot.plot(food_segmented,'b')
        plot.plot(feed_frac_list, 'k')
        plot.set_title('patch (black), food segment (blue), larvae (green) \n')
        plot.set_xlabel('image nr')
        plot.set_ylabel('mean feed')

        plot = self.figure.add_subplot(2, 3, 4)
        plot.imshow(overlay)
        plot.set_title('Food patches overlay with attention contoured  \n on rectified image \n ')
        plot.axis('off')

        plot = self.figure.add_subplot(2, 3, 5)

        plot.imshow(segmentation_overlay)
        plot.set_title('Segmentation food (blue) larvae (green) \n with attention contoured  \n on rectified image \n')
        plot.axis('off')

        plot = self.figure.add_subplot(2, 3, 6)
        plot.imshow(black_larvae)
        plot.set_title('Spots for possible black larvae detections \n')
        plot.axis('off')

        self.figure.canvas.draw()
        # plt.show(block = False)
        # plt.pause(1)
        figure_path_name = join(self.save_path, name)
        self.figure.savefig(figure_path_name)
        w, h = self.figure.canvas.get_width_height()
        buf = np.fromstring(self.figure.canvas.tostring_rgb(), dtype=np.uint8)
        buf.shape = (h, w, 3)

        image = cv2.cvtColor(buf, cv2.COLOR_RGB2BGR)

        return image, thermal_map

if __name__ == "__main__":

    # main_folder = expanduser('/media/rolf/DATA_PURPLE/big_box3/')
    main_folder = '/media/rolf/B627-9CE9/Data/big_box4'
    main_folder = '/media/rolf/SWAPDISK2'
    # main_folder = expanduser('/mnt/C5A5-82AB/Data/big_box2')

    csv_name = 'measurements.csv'
    feed_map_sub_dir = 'feedmap'
    sub_folders = [join(main_folder,x) for x in listdir(main_folder) if isdir(join(main_folder,x,feed_map_sub_dir))]
    image_sub_dir = 'recordings'
    files_to_skip = 0

    for sub_folder in sub_folders:
        viz = FeedProcessVisualizer(sub_folder, csv_name, feed_map_sub_dir, image_sub_dir,files_to_skip)
        viz.get_corresponding_data()
