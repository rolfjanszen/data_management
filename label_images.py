import sys
import time
from argparse import ArgumentParser
from datetime import date
from os import listdir, makedirs
from os.path import expanduser, join, isdir
from os.path import isfile, split
from pymongo import MongoClient
import cv2
import numpy as np
import pandas as pd
import yaml
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QCheckBox, QLineEdit, QLabel, QSlider, QWidget, QLCDNumber, \
    QRadioButton, QPushButton, QMessageBox
from tools.general.helpers import timestamp_from_data_time

from browse_recordings import SLASH, FeedSessDisplay
from check_preformance import CheckPerformance

DO_ALL_CONTAINERS = 'All'

base_link = 'https://entomics.sharepoint.com/sites/Engineering/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FEngineering%2FShared%20Documents%2FProblematic%20trays%2F'
middle_part_url = '&parent=%2Fsites%2FEngineering%2FShared%20Documents%2FProblematic%20trays%2F'


class TriButton(QCheckBox):
    has_3_states = False
    set_data_add = 1

    def __init__(self):
        super(TriButton, self).__init__("")
        if not self.has_3_states:
            self.set_data_add = 0
        self.setTristate(self.has_3_states)

    def get_label_state(self):
        if not self.has_3_states:
            if self.checkState() == 0:
                return None
            elif self.checkState() == 1:
                return True

        if self.checkState() == 0:
            return None
        elif self.checkState() == 1:
            return False
        elif self.checkState() == 2:
            return True

    def setFromData(self, data_set, val_name):
        if val_name not in data_set:
            self.setCheckState(0)
            print("could not find", val_name, "in data set")
            return
        val = data_set[val_name]
        if val == None or val != val:
            self.setCheckState(0)
        else:
            self.setCheckState((int(val) + self.set_data_add))


class SpecialSlider(QSlider):
    max_range = 10

    def __init__(self, max_range_: int, slide_width_: int):
        super(SpecialSlider, self).__init__(Qt.Horizontal)
        self.setTickInterval(10)
        self.max_range = max_range_
        self.setRange(-1, max_range_)
        self.setValue(-1)
        self.setFixedWidth(slide_width_)

    def setValData(self, data_set, val_name, multiplier):
        if val_name not in data_set:
            self.setValue(-1)
            print("could not find", val_name, "in data set")
            return
        val = data_set[val_name]
        if val == None or val != val:
            self.setValue(-1)
        else:
            self.setValue(int(val * multiplier))

    def increseStep(self):
        new_val = self.value()
        new_val += 10

        new_val = min(new_val, self.max_range)
        self.setValue(new_val)

    def decreaseStep(self):
        new_val = self.value()
        new_val -= 10
        if new_val < 0:
            new_val = -1
        self.setValue(new_val)


class GetSetRadioButton(QRadioButton):

    def __init__(self, button_name, display_name, event_connect):
        super(GetSetRadioButton, self).__init__(button_name)
        self.country = display_name
        self.toggled.connect(event_connect)

    def get_state(self):
        if self.isChecked():
            return True
        else:
            return None

    def set_state(self, data_set, val_name):
        if val_name not in data_set:
            print("could not find", val_name, "in data set")
            return
        val = data_set[val_name]
        if val:
            self.click()


class RecordingsLabeler(CheckPerformance):
    max_t_diff = 3
    store_entry = -1
    excel_name = 'bad_trays.xlsx'
    date_key = "%Y-%m-%d_%H_%M_%S"
    nr_labels_set = 0
    trays_seen = 0

    def __init__(self, date_sub, im_path, store_vids, label_file, labeler_id, containers_selected,
                 container_confs_path, save_excel, end_date_, on_local_folder, only_fed, no_db_upload,*args,
                 **kwargs):

        super(RecordingsLabeler, self).__init__(date_sub, im_path, store_vids, *args, **kwargs)
        self.container_confs_path = container_confs_path
        self.save_as_excel = save_excel
        self.labeler_id = labeler_id
        self.im_path = expanduser(im_path)
        self.save_folder_name = str(date.today())
        self.only_fed = only_fed
        self.label_folder = expanduser('~/Data/')
        self.screen_save_path = join(self.label_folder, self.save_folder_name)
        self.no_db_upload = no_db_upload

        if not isdir(self.label_folder):
            makedirs(self.label_folder)

        self.store_vids = store_vids
        if not isdir(self.screen_save_path):
            makedirs(self.screen_save_path)

        full_date = '20' + date_sub + '_00_00'
        end_date = '20' + end_date_ + '_00_00'

        self.start_date = timestamp_from_data_time(full_date, self.date_key)
        self.end_date = timestamp_from_data_time(end_date, self.date_key)
        # print("full_date", full_date, self.start_date, self.date_key)
        label_file = str(date.today())[:7] + label_file
        excel_name = 'trays_reviewed.xlsx'
        tray_health_name = 'tray_health.csv'
        if len(containers_selected) == 1:
            # label_file = containers_selected[0] + label_file
            excel_name = containers_selected[0] + excel_name
            tray_health_name = containers_selected[0] + tray_health_name
        self.excel_name = join(self.screen_save_path, excel_name)
        self.health_age_path = join(self.screen_save_path, tray_health_name)
        self.label_file = join(self.label_folder, label_file)

        self.containers_selected = containers_selected

        self.curr_feed_sess = FeedSessDisplay('Tray to label', 0.7)

        # self.next_feed_sess = FeedSessDisplay('Tray at later state', 0.5)
        self.data_columns = ['base_folder', 'sub_folder', 'feed', 'wetness', 'tray_id',
                             'time_stamp', 'container', 'is_ht_substrate', 'date', 'iteration',
                             'vid_sequence', 'image', 'dryness_level', 'rectify_map', 'crop_area',
                             'rescale', 'low_seed', 'age', 'bad_tray', 'pre-pupea', 'mounding',
                             'give_food', 'diseased_larvae', 'comments', 'org_feed_mod', 'labeler',
                             'feed_more', 'perma_stop_feed', 'skip 24h', 'skip 48h', 'discard', 'suspicious', 'stressed'
                             'stack', 'trays_seen', 'link', 'label_date', 'no_image']

        if isfile(self.label_file):
            self.df = pd.read_csv(self.label_file, index_col=False)
        else:
            self.df = pd.DataFrame(columns=self.data_columns)

        entry_start_today = self.df[self.df['label_date'] == str(date.today())].first_valid_index()
        if self.df.shape[0] < 1:
            self.start_entry = 0
        elif entry_start_today is None:
            self.start_entry = self.df.shape[0]
        else:
            self.start_entry = entry_start_today

        self.feed_mod_layout = QtWidgets.QHBoxLayout()
        self.checkbox_layout = QtWidgets.QHBoxLayout()
        self.fit_dry_layout = QtWidgets.QHBoxLayout()
        self.tray_decision_layout = QtWidgets.QHBoxLayout()
        self.comment_layout = QtWidgets.QHBoxLayout()

        # mod_lbl.setFixedWidth(100)
        # dryness_lbl = QLabel("Too dry:")

        diseased_lbl = QLabel("Diseased (t):")
        dryness_scale_lbl = QLabel("Dryness 1-5: (wet) ")
        dry_scale_lbl = QLabel("(very dry)")
        container_lbl = QLabel("Container")
        self.container_display = QLabel()
        tray_lbl = QLabel("Tray nr:")
        stack_lbl = QLabel("stack id:")
        cumm_food_lbl = QLabel("Cumulative current tray (ml):")
        tray_percent_lbl = QLabel("Trays seen (%)")
        tray_counter_lbl = QLabel("Tray count")

        radio_layout = QtWidgets.QGridLayout()
        self.but_del_label = QPushButton("Delete label")
        self.but_del_label.clicked.connect(self.delete_label)
        self.but_feed_y_n = TriButton()

        self.but_good_tray = GetSetRadioButton("Good tray(f)", "good_tray", self.onClicked)
        radio_layout.addWidget(self.but_good_tray, 0, 0)

        self.but_feed_more = GetSetRadioButton("Feed more (g)", "feed_more", self.onClicked)
        self.but_feed_more.setChecked(True)
        radio_layout.addWidget(self.but_feed_more, 0, 1)

        self.but_skip_24h = GetSetRadioButton("Skip feeding 24h (h)", "skip_feed_24",
                                              self.onClicked)
        radio_layout.addWidget(self.but_skip_24h, 0, 2)


        self.but_record_failed = GetSetRadioButton("Recording failed (j)", "rec_failed",
                                                   self.onClicked)
        radio_layout.addWidget(self.but_record_failed, 0, 3)

        self.but_discard = GetSetRadioButton("Discard tray (k)", "Discard", self.onClicked)
        radio_layout.addWidget(self.but_discard, 0, 4)

        self.but_suspicious = GetSetRadioButton("Suspicious (l)", "suspicious",
                                                     self.onClicked)
        radio_layout.addWidget(self.but_suspicious, 0, 5)

        self.but_stressed = GetSetRadioButton("Stressed (m)", "stressed",
                                                     self.onClicked)
        radio_layout.addWidget(self.but_stressed, 0, 6)

        self.but_perma_stop_feed = GetSetRadioButton("Perma stop feed (n)", "perma_stop_feed",
                                                     self.onClicked)
        radio_layout.addWidget(self.but_perma_stop_feed, 0, 7)
        radio_layout.addWidget(self.but_del_label)
        self.connect_to_cloud()

        self.tray_nr = QLabel()
        self.stack_id = QLabel()
        self.tray_t_diff = QLabel()
        self.tray_count_seen = QLabel()
        self.percent_trays_seen = QLabel()
        time_diff_lbl = QLabel("Recordings time difference (hours):")
        self.t_diff_display = QLCDNumber()
        self.t_diff_display.setSegmentStyle(QLCDNumber.Flat)
        self.cumm_food = QLCDNumber()
        self.cumm_food.setSegmentStyle(QLCDNumber.Flat)
        tray_info_layout = QtWidgets.QHBoxLayout()
        tray_info_layout.addWidget(cumm_food_lbl)
        tray_info_layout.addWidget(self.cumm_food)
        tray_info_layout.addWidget(tray_counter_lbl)
        tray_info_layout.addWidget(self.tray_count_seen)
        tray_info_layout.addWidget(tray_percent_lbl)
        tray_info_layout.addWidget(self.percent_trays_seen)
        tray_info_layout.addWidget(container_lbl)
        tray_info_layout.addWidget(self.container_display)
        tray_info_layout.addWidget(tray_lbl)
        tray_info_layout.addWidget(self.tray_nr)
        tray_info_layout.addWidget(stack_lbl)
        tray_info_layout.addWidget(self.stack_id)
        # tray_info_layout.addWidget(time_diff_lbl)
        # tray_info_layout.addWidget(self.t_diff_display)

        seed_lbl = QLabel("Little seed (l):")
        bad_tray_lbl = QLabel("Bad tray (b):")
        pre_pupea_lbl = QLabel("Pre pupae (p):")
        mounding_lbl = QLabel("Mounding (m):")
        feed_y_n_lbl = QLabel("Add feed y/n (y)")

        self.but_seed = TriButton()
        self.but_bad_tray = TriButton()
        self.but_pre_pupae = TriButton()
        self.but_mounding = TriButton()
        self.but_diseased = TriButton()

        self.tray_decision_layout.addWidget(self.but_feed_more)
        self.tray_decision_layout.addWidget(self.but_skip_24h)
        self.tray_decision_layout.addWidget(self.but_record_failed)
        self.tray_decision_layout.addWidget(self.but_discard)
        self.tray_decision_layout.addWidget(self.but_suspicious)
        self.tray_decision_layout.addWidget(self.but_stressed)
        self.tray_decision_layout.addWidget(self.but_perma_stop_feed)

        self.feed_mod_disp = QLineEdit(self)
        self.feed_mod_disp.setFixedWidth(80)
        comment_lbl = QLabel("Please enter extra comments: ")
        self.comment_on_tray = QLineEdit(self)
        self.comment_layout.addWidget(comment_lbl)
        self.comment_layout.addWidget(self.comment_on_tray)
        self.feed_mod_sliders = SpecialSlider(max_range_=300, slide_width_=250)
        self.dryness_scale = SpecialSlider(max_range_=50, slide_width_=150)

        self.set_feed_mod_slide()

        self.feed_mod_sliders.setFixedWidth(600)
        mod_lbl = QLabel("Feed modifier (1,2,3,4,+,-)")
        self.feed_mod_layout.addWidget(mod_lbl)
        self.feed_mod_layout.addWidget(self.feed_mod_sliders)
        self.feed_mod_layout.addWidget(self.feed_mod_disp)
        # # self.feed_mod_layout.insertSpacing(-1, 600)
        # self.feed_mod_layout.addWidget(dryness_scale_lbl)
        # self.feed_mod_layout.addWidget(self.dryness_scale)
        # self.feed_mod_layout.addWidget(dry_scale_lbl)

        self.checkbox_layout.addWidget(bad_tray_lbl)
        self.checkbox_layout.addWidget(self.but_bad_tray)
        self.checkbox_layout.addWidget(diseased_lbl)
        self.checkbox_layout.addWidget(self.but_diseased)
        self.checkbox_layout.addWidget(self.but_bad_tray)
        self.checkbox_layout.addWidget(seed_lbl)
        self.checkbox_layout.addWidget(self.but_seed)
        self.checkbox_layout.addWidget(pre_pupea_lbl)
        self.checkbox_layout.addWidget(self.but_pre_pupae)
        self.checkbox_layout.addWidget(mounding_lbl)
        self.checkbox_layout.addWidget(self.but_mounding)
        self.checkbox_layout.addWidget(feed_y_n_lbl)
        self.checkbox_layout.addWidget(self.but_feed_y_n)

        main_layout = QtWidgets.QVBoxLayout()
        feed_sess_compare_layout = QtWidgets.QHBoxLayout()
        feed_sess_compare_layout.addLayout(self.curr_feed_sess)
        # feed_sess_compare_layout.addLayout(self.next_feed_sess)
        main_layout.addLayout(self.file_layout)
        main_layout.addLayout(tray_info_layout)
        main_layout.addLayout(self.layout)
        main_layout.addLayout(feed_sess_compare_layout)
        main_layout.addLayout(self.information_layout)
        main_layout.addLayout(self.tray_decision_layout)
        main_layout.addLayout(radio_layout)
        main_layout.addLayout(self.cntrl_layout)
        main_layout.addLayout(self.feed_mod_layout)
        # main_layout.addLayout(self.checkbox_layout)
        # main_layout.addLayout(self.fit_dry_layout)
        main_layout.addLayout(self.comment_layout)
        # self.connect(self, Qt.SIGNAL('triggered()'), self.closeEvent)
        self.feed_mod_sliders.sliderMoved.connect(self.set_feed_mod_slide)

        self.save_image_button.clicked.connect(self.store_label)
        self.save_image_button.setText("Store label/ next")
        self.widget = QWidget()
        self.widget.setLayout(main_layout)
        self.setCentralWidget(self.widget)
        self.on_local_folder = on_local_folder
        self.load_results()
        self.load_next()

    def onClicked(self):
        radioButton = self.sender()

    def closeEvent(self, event):
        self.save_df_to_file()
        event.accept()
        self.disconnect_cloud()

    def delete_label(self):
        if self.store_entry >= 0:
            self.df.drop([self.store_entry], inplace=True)
            if self.store_entry < self.start_entry:
                self.start_entry -= 1
            # self.save_df_to_file()
            self.reset_labels()

    def disp_cummulative_feed(self):
        if 'food_pumped' not in self.df_tray:
            self.cumm_food.display('0')
            return
        food_column = self.df_tray['food_pumped'][:(self.in_tray_counter + 1)]

        summed_food = sum(food_column.dropna().to_list())
        self.cumm_food.display(summed_food)

    def set_feed_mod_slide(self):
        feed_mod = self.feed_mod_sliders.value()
        feed_mod_class = feed_mod / 200
        if feed_mod_class < 0:
            feed_mod_class = None
        self.feed_mod_disp.setText(str(feed_mod_class))

    def make_clickable(self, val):
        # target _blank to open new window
        return '<a target="_blank" href="{}">{}</a>'.format(val, val)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_1:
            self.feed_mod_sliders.setValue(0)
            self.feed_mod_disp.setText(str(0))
        elif event.key() == Qt.Key_2:
            self.feed_mod_sliders.setValue(100)
            self.feed_mod_disp.setText(str(0.5))
        elif event.key() == Qt.Key_3:
            self.feed_mod_sliders.setValue(200)
            self.feed_mod_disp.setText(str(1))
        elif event.key() == Qt.Key_4:
            self.feed_mod_sliders.setValue(300)
            self.feed_mod_disp.setText(str(1.5))
        elif event.key() == Qt.Key_Plus:
            self.feed_mod_sliders.increseStep()
            new_val = self.feed_mod_sliders.value()
            self.feed_mod_disp.setText(str(new_val / 200))
        elif event.key() == Qt.Key_Minus:
            self.feed_mod_sliders.decreaseStep()
            new_val = self.feed_mod_sliders.value()
            self.feed_mod_disp.setText(str(new_val / 200))
        elif event.key() == Qt.Key_X:
            self.dryness_scale.increseStep()
        elif event.key() == Qt.Key_Z:
            self.dryness_scale.decreaseStep()
        elif event.key() == Qt.Key_S:
            self.store_label()
        elif event.key() == Qt.Key_W:
            self.but_wetness.click()
        elif event.key() == Qt.Key_C:
            self.but_seed.click()
        elif event.key() == Qt.Key_T:
            self.but_diseased.click()
        elif event.key() == Qt.Key_B:
            self.but_bad_tray.click()
        elif event.key() == Qt.Key_P:
            self.but_pre_pupae.click()
        elif event.key() == Qt.Key_Y:
            self.but_feed_y_n.click()
        elif event.key() == Qt.Key_F:
            self.but_good_tray.click()
        elif event.key() == Qt.Key_G:
            self.but_feed_more.click()
        elif event.key() == Qt.Key_H:
            self.but_skip_24h.click()
        elif event.key() == Qt.Key_J:
            self.but_record_failed.click()
        elif event.key() == Qt.Key_K:
            self.but_discard.click()
        elif event.key() == Qt.Key_L:
            self.but_suspicious.click()
        elif event.key() == Qt.Key_M:
            self.but_stressed.click()
        elif event.key() == Qt.Key_N:
            self.but_perma_stop_feed.click()
        elif event.key() == Qt.Key_D:
            self.load_next()
        elif event.key() == Qt.Key_A:
            self.load_prev()
        # elif event.key() == Qt.Key_Q:
        #     self.save_image_to_age()
        #     if self.counter > 1:
        #         self.load_next()

    def save_image_to_age(self):
        if self.age > 8:
            return
        tray_age_str = 'day_' + str(int(self.age))
        # print(len(tray_age_str), tray_age_str)False
        if not len(tray_age_str):
            print("no known age")
            return
        if not isdir(tray_age_str):
            makedirs(tray_age_str)
        file_name_split = self.video_file.split(SLASH)
        new_im_name = str(self.tray['nr']) + '_' + file_name_split[-1][:-3] + 'webp'
        # print(len(tray_age_str), tray_age_str)
        new_save_path = join(tray_age_str, new_im_name)

        cap = cv2.VideoCapture(self.video_file)
        ret, frame = cap.read()
        if not ret:
            return
        frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        cv2.imwrite(new_save_path, frame)
        # print('saved new image to', new_save_path)

    def save_video_still_make_url(self):
        cap = cv2.VideoCapture(self.video_file)
        ret, frame = cap.read()
        if not ret:
            return 'failed saving image'
        frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        split_video_name = self.video_file.split(SLASH)

        save_name = split_video_name[-4] + '_' + \
                    self.tray['nr'] + '_' + \
                    split_video_name[-1][:-3] + 'webp'

        full_save_path = join(self.screen_save_path, save_name)
        cv2.imwrite(full_save_path, frame)

        return base_link + self.save_folder_name + '%2F' + save_name + middle_part_url + self.save_folder_name

    def set_trays_seen(self):
        data_length = self.df.shape[0] - 1
        self.df.at[data_length, 'trays_seen'] = self.trays_seen
        # self.save_df_to_file()

    def disconnect_cloud(self):
        self.online_client.close()

    def connect_to_cloud(self):
        uri = "mongodb+srv://containercluster.pkphq.mongodb.net/myFirstDatabase?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority"
        path_cert = 'labelling_user.pem'
        self.online_client = MongoClient(uri, tls=True, tlsCertificateKeyFile=path_cert)
        self.cloud_coll = self.online_client['Labels']['monitoring']

    def store_label(self):
        feed_mod = self.feed_mod_sliders.value()
        feed_mod_class = feed_mod / 100
        if feed_mod_class < 0:
            feed_mod_class = None

        # # is_too_dry = self.but_dry.isChecked()
        has_little_seed = self.but_seed.get_label_state()
        is_diseased = self.but_diseased.get_label_state()
        is_bad_tray = self.but_bad_tray.get_label_state()
        is_mounding = self.but_mounding.get_label_state()
        give_more_food = self.but_feed_y_n.get_label_state()
        dry_level = self.dryness_scale.value()
        good_tray = self.but_good_tray.isChecked()
        feed_more = self.but_feed_more.isChecked()
        skip_feed_24h = self.but_skip_24h.isChecked()
        record_failed = self.but_record_failed.isChecked()
        discard_tray = self.but_discard.isChecked()
        suspicious_tray = self.but_suspicious.isChecked()
        stressed_tray = self.but_stressed.isChecked()
        perma_stop_feed = self.but_perma_stop_feed.isChecked()

        if skip_feed_24h or record_failed or discard_tray or suspicious_tray or stressed_tray or perma_stop_feed:
            is_bad_tray = True

        stack_id = self.stack_id.text()
        dry_level = dry_level / 10
        if dry_level < 0:
            dry_level = None
        is_pre_pupae = self.but_pre_pupae.get_label_state()

        comments = self.comment_on_tray.text()
        file_name_split = self.video_file.split(SLASH)
        file_name = file_name_split[-1]
        vid_date = file_name[:19]
        data_path = split(self.video_file)[0]
        # data_path = join('/', *self.video_file.split(SLASH)[:-1])

        container_base_folder = split(split(data_path)[0])[0]
        # config_file = join(container_base_folder, self.container_confs_path, 'config.yaml')
        config_file = join(self.im_path, self.container_confs_path, self.container_name,
                           'config.yaml')
        # self.time_stamp = timestamp_from_data_time(file_name[:19], "%Y-%m-%d_%H_%M_%S")
        with open(config_file, 'r') as file:

            config_data = yaml.load(file, Loader=yaml.FullLoader)
            crop_area = config_data['crop_feeder_analyses']
            re_scale = config_data['scale_feeder_analyses']
            rectify_map = self.container_name + '.npy'
            new_label_row = dict.fromkeys(self.data_columns)
            new_label_row['base_folder'] = data_path
            new_label_row['sub_folder'] = file_name
            new_label_row['feed'] = feed_mod_class
            new_label_row['rectify_map'] = rectify_map
            new_label_row['crop_area'] = crop_area
            new_label_row['rescale'] = re_scale
            new_label_row['time_stamp'] = self.time_stamp
            new_label_row['dryness_level'] = dry_level
            new_label_row['low_seed'] = has_little_seed
            new_label_row['mounding'] = is_mounding
            new_label_row['pre-pupea'] = is_pre_pupae
            new_label_row['diseased_larvae'] = is_diseased
            new_label_row['give_food'] = give_more_food
            new_label_row['bad_tray'] = is_bad_tray
            new_label_row['labeler'] = self.labeler_id
            new_label_row['comments'] = comments
            new_label_row['age'] = self.age
            new_label_row['org_feed_mod'] = self.class_signal
            new_label_row['date'] = vid_date
            new_label_row['container'] = self.container_name
            new_label_row['iteration'] = file_name_split[-3].split('_')[-1]
            new_label_row['tray_id'] = self.tray['nr']
            new_label_row['skip 24h'] = skip_feed_24h
            new_label_row['no_image'] = record_failed
            new_label_row['discard'] = discard_tray
            new_label_row['suspicious'] = suspicious_tray
            new_label_row['stressed'] = stressed_tray
            new_label_row['perma_stop_feed'] = perma_stop_feed
            new_label_row['stack'] = stack_id
            new_label_row['trays_seen'] = self.trays_seen
            new_label_row['feed_more'] = feed_more
            new_label_row['good'] = good_tray
            new_label_row['label_date'] = str(date.today())

            # print('self.store_entry', self.store_entry)
            if self.store_entry >= 0 and self.store_entry < self.start_entry:
                container_name = self.df[self.store_entry]['container']
                tray_date = self.df[self.store_entry]['date']
                self.df.drop([self.store_entry], inplace=True)
                self.start_entry -= 1
                self.store_entry = -1

            if is_bad_tray:
                url_link = self.save_video_still_make_url()
                new_label_row['link'] = url_link

            if self.store_entry >= 0:
                self.df.iloc[self.store_entry] = pd.Series(new_label_row)
            else:
                self.df = self.df.append(new_label_row, ignore_index=True)

            # self.df.style.format({'link': self.make_clickable})
            # self.save_df_to_file()
            self.nr_labels_set += 1
            # if self.nr_labels_set % 50 == 0:
            #     self.save_df_to_file()

        self.load_next()

    def upload_to_mongo_atlas(self):
        if self.no_db_upload:
            return

        print("uploading labels to database, please wait")
        t_stamp_24 = time.time() - 48 * 3600
        recent_label_subset = self.cloud_coll.find({'label_t': {'$gt': t_stamp_24}})

        for i, entry in self.excel_df.iterrows():
            entry['label_t'] = time.time()
            recent_label_query = {'container': entry['container'], 'date': entry['date']}
            if recent_label_subset.collection.count_documents(recent_label_query):
                continue
            entry['labeler_id'] = self.labeler_id
            entry = entry.drop('link')
            self.cloud_coll.insert_one(entry.to_dict())

    def get_tray_health_by_age(self):
        all_entries = []

        for age in range(13):
            df = self.excel_df[self.excel_df['age'] == age]
            total_len = df.shape[0]
            if not total_len:
                continue

            new_entry = dict({'age': age})

            for label in ['good', 'feed_more', 'skip 24h', 'no_image', 'perma_stop_feed', 'discard','suspicious','stressed']:
                labels_true = df[df[label].astype(str) == 'True'].shape[0]
                new_entry[label] = round(labels_true / total_len, 2) if labels_true else 0
            all_entries.append(new_entry)

        df_health_age = pd.DataFrame(all_entries)
        df_health_age.to_csv(self.health_age_path)

    def save_df_to_file(self):
        self.df.to_csv(self.label_file, index=False)
        if not self.df.shape[0]:
            return
        if self.save_as_excel:
            self.excel_df = self.df.iloc[self.start_entry:][
                ['date', 'container', 'tray_id', 'iteration', 'stack', 'age', 'good',
                 'feed_more', 'skip 24h', 'no_image', 'perma_stop_feed',
                 'discard','suspicious','stressed', 'link', 'time_stamp']]
            if self.containers_selected != DO_ALL_CONTAINERS and 'container' in self.excel_df:
                container_related = [c in self.containers_selected for c in
                                     self.excel_df['container']]
                self.excel_df = self.excel_df[container_related]
            if not self.excel_df.shape[0]:
                return
            self.excel_df = self.excel_df.sort_values(['container', 'stack'])
            self.excel_df['link'].apply(lambda x: self.make_hyperlink(x))
            self.excel_df.to_excel(self.excel_name)
            self.get_tray_health_by_age()
            self.upload_to_mongo_atlas()

    def make_hyperlink(self, value):
        url = "https://custom.url/{}"
        return '=HYPERLINK("%s", "%s")' % (url.format(value), value)

    def load_image(self):
        line_data = self.df_tray.iloc[self.in_tray_counter]

        if self.only_fed and ('feed_modifier' not in line_data or pd.isna(line_data['feed_modifier'])):
            print('not good image self.only_fed',self.only_fed)
            # print('feed_modifier not in line',('feed_modifier' not in line_data))
            # print('pd.isna(line_data',pd.isna(line_data['feed_modifier']))
            return False

        # if line_data['tray_age'] < 1:
        #     return False

        self.disp_cummulative_feed()

        if "stack_id" in line_data:
            self.stack_id.setText(str(line_data["stack_id"]))
        else:
            self.stack_id.setText("unknown")

        t_stamp = timestamp_from_data_time(line_data['Time and date'], '%Y-%m-%d %H:%M:%S')
        self.time_stamp = t_stamp
        self.last_t_stamp = t_stamp
        im_entry, t_diff = self.get_closest_file_t_stamp(t_stamp, self.recordings_t_stamps)

        if im_entry < 0 or t_diff > self.max_t_diff or t_stamp < self.start_date or t_stamp > self.end_date:
            print('wrong time stamp out of date')
            return False

        t_stamp_vid_next = 0
        trays_seen_percent = str(round(100 * self.counter / self.len_data_to_browse)) + '%'
        self.percent_trays_seen.setText(trays_seen_percent)
        # print("lookin in folder ", self.counter, 'of', len(self.base_folders))

        im = self.recorded_ims[im_entry]
        self.video_file = im
        im_name_split = im.split(SLASH)
        t_stamp_vid = timestamp_from_data_time(im_name_split[-1][:19], '%Y-%m-%d_%H_%M_%S')
        self.file_path_shown.setText(im)
        self.tray_nr.setText(self.tray['nr'])
        self.container_display.setText(im_name_split[-4])

        im_message = ''
        if line_data['food_pumped'] < 10:
            im_message = 'tray not fed'
        self.curr_feed_sess.display_tray_images(im,im_message)
        self.curr_feed_sess.display_tray_info(line_data)
        self.tray_t_diff.setText('time diff')
        self.age = line_data['tray_age']
        self.container_name = im_name_split[-4]
        if 'feed_modifier' in line_data:
            self.class_signal = line_data['feed_modifier']
            self.modidfier = line_data['feed_modifier']
        else:
            self.class_signal = -1
            self.modidfier = -1

        self.tray_count_seen.setText(str(self.trays_seen))
        self.match_labels()

        t_diff = int(t_stamp_vid_next - t_stamp_vid) / 3600
        self.t_diff_display.display(t_diff)
        return True

    def match_labels(self):

        if not self.df.shape[0]:
            return

        diff_label_entry = np.abs(np.array(self.df.time_stamp) - self.time_stamp)
        label_entry = diff_label_entry.argmin()
        # print("label entry",label_entry)

        try:
            # should always work, until someone starts bashing buttons
            min_val = abs(self.df.time_stamp[label_entry] - self.time_stamp)
        except Exception as e:
            print(e)
            return
        set_data = self.df.iloc[label_entry]
        if min_val < 30 and set_data['container'] == self.container_name:

            # self.but_dry.setChecked(  int(set_data.too_dry))
            self.but_seed.setFromData(set_data, 'low_seed')
            self.but_pre_pupae.setFromData(set_data, 'pre-pupea')
            self.but_diseased.setFromData(set_data, 'diseased_larvae')
            self.but_bad_tray.setFromData(set_data, 'bad_tray')
            self.but_mounding.setFromData(set_data, 'mounding')
            self.but_pre_pupae.setFromData(set_data, 'pre-pupea')
            self.but_feed_y_n.setFromData(set_data, 'give_food')
            self.dryness_scale.setValData(set_data, 'dryness_level', 10)
            self.feed_mod_sliders.setValData(set_data, 'feed', 100)

            self.but_skip_24h.set_state(set_data, 'skip 24h')
            self.but_record_failed.set_state(set_data, 'no_image')
            self.but_discard.set_state(set_data, 'discard')
            self.but_stressed.set_state(set_data, 'stressed')
            self.but_suspicious.set_state(set_data, 'suspicious')
            self.but_feed_more.set_state(set_data, 'feed_more')
            self.but_perma_stop_feed.set_state(set_data,'perma_stop_feed')

            if set_data.feed is None:
                self.feed_mod_disp.setText(str(None))
                self.feed_mod_sliders.setValue(-1)
            else:
                self.feed_mod_disp.setText(str(set_data.feed / 2))

            if not pd.isna(set_data.comments):
                self.comment_on_tray.setText(set_data.comments)

            self.store_entry = label_entry
        else:
            self.reset_labels()

    def reset_labels(self):

        self.but_bad_tray.setChecked(0)
        self.but_seed.setChecked(0)
        self.but_feed_y_n.setChecked(0)

        self.comment_on_tray.setText('')
        self.but_pre_pupae.setChecked(0)
        self.but_mounding.setChecked(0)
        self.but_diseased.setChecked(0)

        # self.dryness_scale.setValue(-1)
        self.feed_mod_sliders.setValue(-1)
        self.set_feed_mod_slide()
        self.store_entry = -1
        self.but_feed_more.click()

    def load_results(self):

        self.data_to_browse = []
        self.base_folders = []

        if self.on_local_folder:
            self.base_folders = listdir(self.im_path)
        else:
            container_folders = self.containers_selected
            if self.containers_selected == DO_ALL_CONTAINERS:
                container_folders = listdir(self.im_path)

            for container_folder in container_folders:
                new_path = join(self.im_path, container_folder)
                if not isdir(new_path):
                    continue

                tray_folders = listdir(new_path)
                self.base_folders += [join(new_path, d) for d in tray_folders]

        # print("base_folders", self.base_folders)
        for base_folder in self.base_folders:
            got_tray = self.get_trays_analyses_folder(base_folder)
            if got_tray:
                break

        if not len(self.data_to_browse):
            print("ERROR. Could not find trays older than ", self.start_date)
            sys.exit(0)
        self.len_data_to_browse = len(self.base_folders)
        print("found ", self.len_data_to_browse, 'folders')

        for base_folder in self.base_folders:
            if self.get_trays_analyses_folder(base_folder):
                break


if __name__ == '__main__':
    parser = ArgumentParser()

    app = QtWidgets.QApplication(sys.argv)
    parser.add_argument('-t', '--date_time',
                        help="Set day month and hour as follows: YY-MM-DD_hh",
                        type=str, required=False, default='21-08-10_12')
    parser.add_argument('-e', '--end_date', help="max date for trays",
                        type=str, required=False, default='99-08-10_12')
    parser.add_argument('-p', '--path', help="path to files",
                        type=str, required=False, default='/media/x1-2/SWAPDISK')
    parser.add_argument('-l', '--labeler', help="Name of the person that is labeling",
                        type=str, required=True)
    parser.add_argument('-v', '--store_vids', help="Also store the videos",
                        type=bool, required=False, default=True)
    parser.add_argument('-f', '--label_file', help="Name of csv file holding labeles data",
                        type=str, required=False, default="tray_labels.csv")
    parser.add_argument('-c', '--container_conf_path', help="path to configuration file container",
                        type=str, required=False, default="config_files")
    parser.add_argument('-s', '--select_containers', nargs='+',
                        help="Which containers do you want to inspect/ label? Default is everyone",
                        type=str, required=False, default=DO_ALL_CONTAINERS)
    parser.add_argument('-x', '--save_excel',
                        help="Also print out an excel for bad tray monitoring as well",
                        type=bool, required=False, default=False)
    parser.add_argument('-o', '--local_folder', help="Save as excel as well",
                        type=bool, required=False, default=False)
    parser.add_argument('-u', '--only_fed',
                        help="Set to True to only show images taken during a feeding session",
                        type=bool, required=False, default=False)
    parser.add_argument('-a', '--no_db_upload',
                        help="Upload to mongo cloud atlas",
                        type=bool, required=False, default=False)

    args = parser.parse_args(sys.argv[1:])
    window = RecordingsLabeler(args.date_time, args.path, args.store_vids, args.label_file,
                               args.labeler, args.select_containers, args.container_conf_path,
                               args.save_excel, args.end_date, args.local_folder, args.only_fed, args.no_db_upload)
    window.show()
    app.exec_()
