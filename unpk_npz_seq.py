import sys
from os.path import join, expanduser

import cv2
import numpy as np

# from cv_bridge import CvBridge

sys.path.append(expanduser('~/scripts/tools'))
from helpers import get_file_list


def unpack_npz(file_path):
    # cv_bridge = CvBridge()
    files = get_file_list(file_path, 'npz')
    for file in files:
        data = np.load(open(join(file_path, file), 'rb'), allow_pickle=True)
        im_arr = data['arr_0']
        print('file',file)
        for i in range(3):
            print(i)
            # try:
            #     im = cv_bridge.imgmsg_to_cv2(im_arr[i])
            # except:
            im =im_arr[i]
            # print(im_arr[i])
            new_file_name = file[:-4] + '_' + str(i) + '.png'
            im = cv2.resize(im, (0, 0), fx=2, fy=2)
            cv2.imwrite(new_file_name, im)

        # print([x for x in data.items()])
        # im_arr = data['arr_0']


if __name__ == "__main__":
    file_path = expanduser('~/Data/wood_farm')
    unpack_npz(file_path)
