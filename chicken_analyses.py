from __future__ import absolute_import, division, print_function

from os.path import expanduser, join, isfile, getsize, isdir
from os import listdir, makedirs
from copy import deepcopy
import sys
import cv2
import numpy as np
import csv
import pandas as pd

osvos_path = expanduser('~/scripts/OSVOS-TensorFlow-master')
pwc_net_path = expanduser('~/scripts/tfoptflow-master/tfoptflow')
sys.path.append(osvos_path)
sys.path.append(pwc_net_path)

from model_pwcnet import ModelPWCNet, _DEFAULT_PWCNET_TEST_OPTIONS
from osvos import get_vos


def create_correction(w, h):
    h_steps = 1 / h
    w_steps = 1 / (0.5 * w)

    height_correction = np.ones((h, w), dtype=np.float32)
    width_correction = np.ones((h, w), dtype=np.float32)

    for y in range(h):
        height_correction[y, :] = 2 - y * h_steps

    half_width = int(0.5 * w)
    width_correction[:, :-half_width] = 1.5
    for x in range(w - half_width):
        width_correction[:, half_width + x] = 1 + x * w_steps

    correction = height_correction * width_correction

    # cv2.imshow('width_correction', (width_correction * 100).astype(np.uint8))
    # cv2.imshow('correction',(correction*50).astype(np.uint8))
    # cv2.waitKey(0)
    return correction


def visualize_flow(flow, flow_mag_max=None, normalize=True):
    hsv = np.zeros((flow.shape[0], flow.shape[1], 3), dtype=np.uint8)
    flow_magnitude, flow_angle = cv2.cartToPolar(flow[..., 0].astype(np.float32), flow[..., 1].astype(np.float32))

    # A couple times, we've gotten NaNs out of the above...
    nans = np.isnan(flow_magnitude)
    if np.any(nans):
        nans = np.where(nans)
        flow_magnitude[nans] = 0.

    # Normalize
    hsv[...,1] = 255
    hsv[..., 0] = flow_angle * 180 / np.pi / 2
    if normalize is True:
        if flow_mag_max is None:
            hsv[..., 2] = cv2.normalize(flow_magnitude, None, 0, 255, cv2.NORM_MINMAX)
        else:
            hsv[..., 2] = flow_magnitude * 255 / flow_mag_max
    else:
        hsv[..., 2] = flow_magnitude

    img = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

    return img


def analyse_motion(video_path, mask_path, csv_name, range_skip, correction_map, nn, csv_headers,flow_vector_save_path):
    mask_video_path = join(video_path, mask_path)
    mask_vid = cv2.VideoCapture(mask_video_path)
    mask_ret, mask_frame = mask_vid.read()
    print('mask_video_path', mask_video_path, mask_ret)
    iter = 0
    summed_avg_velocity = 0
    total_flow = 0

    csv_title = join(video_path, csv_name)
    csv_file = open(csv_title, 'w')
    csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(csv_headers)

    print('analysing', video_path)
    video_cap = cv2.VideoCapture(video_path)
    ret, frame2 = video_cap.read()

    nn.print_config()
    summed_average_mag = 0
    while mask_vid.isOpened() and video_cap.isOpened():
        # for _ in range(range_skip):
        mask_ret, mask_frame = mask_vid.read()

        img_pairs = []
        for i in range(range_skip):
            ret, frame1 = video_cap.read()
        if not ret or not mask_ret:
            print('no frames ', ret, mask_ret)
            break
        img_pairs.append((frame1, frame2))
        pred_flow = nn.predict_from_img_pairs(img_pairs, batch_size=1, verbose=False)
        pred_flow = pred_flow[0]
        flow_vector_name = 'flow_vec_{}.npz'.format(iter)
        frame2 = frame1.copy()

        iter += 1

        vel_magnitude = np.sqrt(pred_flow[..., 0] ** 2 + pred_flow[..., 1] ** 2)
        flow_save_path = join(flow_vector_save_path, flow_vector_name)

        np.savez_compressed(flow_save_path, (vel_magnitude*1000).astype(np.uint32))

        vel_magnitude = cv2.resize(vel_magnitude, (1920, 1024))
        average_mag = np.mean(vel_magnitude)
        magnitude_viz = cv2.normalize(vel_magnitude, None, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)

        magnitude_viz = cv2.resize(magnitude_viz, (0, 0), fx=0.5, fy=0.5)

        flow_frame = visualize_flow(pred_flow)
        w, h = magnitude_viz.shape[:2]
        overlay = np.zeros((w, h, 3), dtype=np.uint8)
        mask_frame = cv2.resize(mask_frame, (1920, 1024))
        mask_frame = cv2.resize(mask_frame, (1920, 1024))

        mask_frame = cv2.cvtColor(mask_frame, cv2.COLOR_BGR2GRAY)
        mask_viz = cv2.resize(mask_frame, (0, 0), fx=0.5, fy=0.5)
        flow_viz = cv2.resize(flow_frame, (0, 0), fx=0.5, fy=0.5)
        ret, magnitude_viz = cv2.threshold(magnitude_viz, 17, 255, cv2.THRESH_BINARY)
        overlay[..., 0] = magnitude_viz
        overlay[..., 1] = mask_viz
        cv2.imshow('magnitude_viz', magnitude_viz)
        cv2.imshow('overlay', overlay)
        cv2.imshow('flow_frame_grey', flow_viz)
        ret, mask_tresh = cv2.threshold(mask_frame, 27, 1, cv2.THRESH_BINARY)
        velocity_corrected = vel_magnitude * mask_tresh.astype(np.float32)
        corrected_mask_flow = velocity_corrected

        cv2.imshow('mask_viz', mask_viz)
        cv2.waitKey(20)


        total_chicken_pix = np.sum(mask_tresh)
        summed_velocity = np.sum(corrected_mask_flow)

        velocity_per_chicken = summed_velocity / total_chicken_pix
        summed_average_mag += average_mag
        summed_avg_velocity += velocity_per_chicken
        csv_writer.writerow(
            [velocity_per_chicken,average_mag, total_chicken_pix, np.sum(velocity_corrected),
             np.sum(corrected_mask_flow)])

        if iter % 500 == 1:
            print(flow_frame.shape, 'velocity_per_chicken', summed_average_mag/iter, iter, 'summed_avg_velocity',
                  summed_avg_velocity/iter, 'total_flow', total_flow)
            # break
    print('summed_avg_velocity', summed_avg_velocity)
    csv_file.close()


def do_analyses(csv_files, csv_headers, room_names):

    extra_headers = ["date", "view_angle"]
    df_headers = extra_headers+csv_headers
    df_set = pd.DataFrame(columns=df_headers)

    for file, room_name in zip(csv_files, room_names):
        df = pd.read_csv(file)
        new_data = [room_name.split(' ')[5],-1]


        for col, header in zip(df.iteritems(), df_headers):
            inliers = col[1][(np.abs((col[1] - col[1].mean()) < 3 * col[1].std()))]
            average_col = inliers.mean()
            new_data.append(average_col)
        df_new = pd.DataFrame([new_data], columns=df_headers, index=[room_name[:6]])
        print(df_new, room_name[:6])
        df_set = df_set.append(df_new)
    return df_set




def run(vid_path, dataset):
    gpu_devices = ['/device:GPU:0']
    controller = '/device:GPU:0'

    # TODO: Set the path to the trained model (make sure you've downloaded it first from http://bit.ly/tfoptflow)
    ckpt_path = './models/pwcnet-lg-6-2-multisteps-chairsthingsmix/pwcnet.ckpt-595000'

    # Configure the model for inference, starting with the default options
    nn_opts = deepcopy(_DEFAULT_PWCNET_TEST_OPTIONS)
    nn_opts['verbose'] = True
    nn_opts['ckpt_path'] = join(pwc_net_path, ckpt_path)
    nn_opts['batch_size'] = 1
    nn_opts['gpu_devices'] = gpu_devices
    nn_opts['controller'] = controller

    # We're running the PWC-Net-large model in quarter-resolution mode
    # That is, with a 6 level pyramid, and upsampling of level 2 by 4 in each dimension as the final flow prediction
    nn_opts['use_dense_cx'] = True
    nn_opts['use_res_cx'] = True
    nn_opts['pyr_lvls'] = 6
    nn_opts['flow_pred_lvl'] = 2

    # The size of the images in this dataset are not multiples of 64, while the model generates flows padded to multiples
    # of 64. Hence, we need to crop the predicted flows to their original size
    nn_opts['adapt_info'] = (1, 1920, 1024, 2)

    correction_map = create_correction(1920, 1024)

    seq_name = "chicken"
    max_training_iters = 400

    checkpoint_path = join(osvos_path, 'models', seq_name, seq_name + '.ckpt-' + str(max_training_iters))
    range_skip = 4
    csv_headers = ['correct velocity/ mask', 'average flow', 'chicken pixels', 'masked velocity',
                   'masked velocity depth corrected']

    csv_file_names = []
    for video_name in dataset:
        video_path = join(vid_path, video_name)
        mask_vid_title = '{}_mask.avi'.format(video_path[:-10])
        overlay_vid_title = '{}_overlay.avi'.format(video_path[:-10])
        csv_name = '{}.csv'.format(video_path[:-10])
        flow_vector_save_path = join(video_path[:-10],'flow_vectors')
        if not isdir(flow_vector_save_path):
            makedirs(flow_vector_save_path)
        # osvos.test(dataset, checkpoint_path, mask_vid_title, overlay_vid_title)
        if not isfile(mask_vid_title) or getsize(mask_vid_title) < 50e6:
            print('getting mask for ', video_path)
            get_vos(video_path, checkpoint_path, mask_vid_title, overlay_vid_title, range_skip)
        if not isfile(csv_name) or getsize(csv_name) < 8e3:
            print('starting motion analyses ', video_path)
            nn = ModelPWCNet(mode='test', options=nn_opts)
            analyse_motion(video_path, mask_vid_title, csv_name, range_skip, correction_map, nn, csv_headers,flow_vector_save_path)

        csv_file_names.append(csv_name)

    df_results = do_analyses(csv_file_names, csv_headers, dataset)
    csv_results_name = join(vid_path,'results.csv')
    df_results.to_csv(csv_results_name)


def get_file_sizes(files_path):
    directories = listdir(files_path)
    video_sizes = dict({})

    for direct in directories:

        if not isdir(join(files_path, direct)):
            continue
        print(direct)
        videos_path = join(files_path, direct)
        videos = [f for f in listdir(videos_path) if f.endswith('.avi')]

        for video in videos:
            video_path = join(videos_path, video)
            video_size = getsize(video_path) / 10e5
            name_split = video.split(' - ')
            time_stamp = name_split[-1][:-4]

            if time_stamp in video_sizes:
                video_sizes[time_stamp].append(video_size)
            else:
                video_sizes[time_stamp] = [video_size]

    print(video_sizes)

    for name, vid_size in video_sizes.items():
        vid_arr = np.array(vid_size)
        vid_arr = vid_arr[vid_arr > 10]
        if vid_arr.shape[0] > 0:
            print(name, np.average(vid_arr), vid_arr.shape[0])
    # df = pd.DataFrame(video_sizes)

    # print(df)


def get_file_list(files_path, time_stamp):
    directories = listdir(files_path)
    videos_to_process = []
    for direct in directories:
        if not isdir(join(files_path, direct)):
            continue
        print(direct)
        videos_path = join(files_path, direct)
        videos = [f for f in listdir(videos_path) if f.endswith('.avi')]
        for video in videos:
            if time_stamp in video:
                videos_to_process.append(join(direct, video))
    return videos_to_process


if __name__ == "__main__":
    vid_path = expanduser('~/Data/chicken_trial/videos')
    dataset = ['RM3 - 15THAUG - 10AM.avi', 'RM4 - 15TH AUG - 10AM.avi']
    vid_path = '/media/overmind/C5A5-82AB/Data/chicken_trial/GENESIS VIDEO'
    dataset = get_file_list(vid_path, '330PM')
    print(dataset)
    # get_file_sizes(vid_path)
    run(vid_path, dataset)
    # exit(0)
