#! /bin/sh

FOLDER=$1
echo "${FOLDER}"
for file in "${FOLDER}"/*; do
	echo ${file}
	if [ -f "${file}" ]; then
		NEW_NAME=$(echo "${file}" | sed -r 's/[:]+/_/g')
		mv ${file} ${NEW_NAME} 
		echo ${NEW_NAME}
	fi

done