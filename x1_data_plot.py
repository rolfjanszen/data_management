import matplotlib.pyplot as plt
from pymongo import MongoClient
from copy import deepcopy
import pandas as pd
from tools.general.helpers import timestamp_from_data_time

def get_tray_data(tray_numbers, queries, collection,start_date):
    all_results = []
    not_real_data = 9999.0

    date_time_name = "Time and date"

    for tray_number in tray_numbers:
        post_id = collection.find({"tray_nr": tray_number})
        id_to_list = [x for x in post_id]

        for latest_tray in id_to_list:
            recordings = latest_tray['records']
            if recordings[0]['time_stamp'] < start_date:
                continue

            new_result = dict({})
            for query in queries:
                new_result[query] = []
            dates = []

            found_data = False

            for record in recordings:
                allgood = [query for query in queries if
                           query in record and record[query] != not_real_data]

                if queries == allgood:
                    dates.append(record[date_time_name][5:13])
                    for query in queries:
                        new_result[query].append(record[query])
                        found_data = True
                # else:
                #     results.append(None)

            if found_data:
                new_result['dates'] = dates
                new_result['tray'] = tray_number
                all_results.append(new_result)

    return all_results


def get_cummulative(results):
    cummulated = 0
    cummulated_arr = []
    for result in results:
        if result > 0:
            cummulated += result
        cummulated_arr.append(cummulated)
    return cummulated_arr

def hours_to_feed_sess(dates):
    new_dates =[]
    print('old dates',dates)
    for new_d in dates:
        hour_int = int(new_d[-2:])
        hour_int = round(hour_int/6)
        new_d= new_d[:-2] + str(hour_int)
        new_dates.append(new_d)
    print('new dates', new_dates)
    return new_dates

def merge_data_arr(results1, results2):
    print("merging data for tray",results1['tray'])
    results1['dates']=hours_to_feed_sess(results1['dates'])
    results2['dates'] = hours_to_feed_sess(results2['dates'])

    df1 = pd.DataFrame(results1)
    df2 = pd.DataFrame(results2)
    merged = pd.merge(df1,df2,'inner','dates')
    if merged.shape[0] < 1 :
        return dict({}), dict({})

    results1['dates']= merged['dates']
    results1['food_pumped'] = merged['food_pumped_x']
    results1['feed_modifier'] = merged['feed_modifier_x']

    results2['dates']= merged['dates']
    results2['food_pumped'] = merged['food_pumped_y']
    results2['feed_modifier'] = merged['feed_modifier_y']

    return results1, results2


    return new_result2

def main(tray_numbers, control_tray,start_date):

    time_stap_start = timestamp_from_data_time(start_date)
    queries = ['food_pumped', 'feed_modifier']
    client = MongoClient('localhost', 27017, serverSelectionTimeoutMS=2000)
    collection = 'trays'
    database_name = 'trays_data'

    collection = client[database_name][collection]
    control_results_master = get_tray_data(control_tray, queries, collection, time_stap_start)[0]
    all_results = get_tray_data(tray_numbers, queries, collection,time_stap_start)


    nr_results = len(all_results)

    y_nr_plits = 4
    fig, ax = plt.subplots(2, y_nr_plits)
    print(nr_results)
    labels = ['food in', 'control', 'modifier']

    fualts = 0
    for i, result_dict in enumerate(all_results):
        control_results = deepcopy(control_results_master)
        i = i-fualts
        plt_y = i % y_nr_plits
        plt_x = int(i / y_nr_plits)
        print(plt_y, plt_x)
        if len(result_dict['food_pumped']) != len(control_results['food_pumped']):
            result_dict,control_results=merge_data_arr(result_dict,control_results)

        if not len(result_dict):
            fualts +=1
            print("faulty data")
            continue
        summed_food = get_cummulative(result_dict['food_pumped'])
        summed_food_cntrl = get_cummulative(control_results['food_pumped'])
        ax2 = ax[plt_x, plt_y].twinx()

        ax2.set_ylabel('modifier (%)')
        # ax.set_ylabel()
        print(result_dict['dates'], summed_food_cntrl)
        ax[plt_x, plt_y].set_title(str(result_dict['tray']))
        print("tray",result_dict['tray'])
        l1 = ax[plt_x, plt_y].plot(result_dict['dates'], summed_food, 'b*-', label=labels[0])
        l2 = ax[plt_x, plt_y].plot(result_dict['dates'], summed_food_cntrl, 'r*-', label=labels[1])
        l3 = ax2.plot(result_dict['dates'], result_dict['feed_modifier'], 'g*', label=labels[2])
        ax[plt_x, plt_y].set_ylim(ymin=0)
        ax2.set_ylim(0, 160)
    for a in ax.flat:
        a.set(xlabel='date (M-D h)', ylabel='cummulated food (ml)')
        plt.setp(a.get_xticklabels(), rotation=30, horizontalalignment='right')
    # plt.legend()
    # fig.tight_layout()
    fig.subplots_adjust(hspace=0.5)
    fig.subplots_adjust(wspace=0.5)
    fig.show()
    # time.sleep(10000)

    plt.show()


if __name__ == "__main__":
    tray_11_ai = [91, 90, 105, 107, 97, 106, 96, 100]

    tray_28_control = [272, 271, 270, 277, 44, 45, 46, 47]
    control_tray_numbers = [102, 101, 104, 93, 94, 92, 95, 103]

    control_tray_2_29 = [18]
    tray_2_ai = [9,8,7,6,5,4,3,2]
    tray_29_ai = [98,88,58,59,89,29,48,49]

    control_tray_6 = [20]
    tray_6_ai = [52,51,50,56,55,65,64,53]
    tray_7_ai = [60,61,62,66,67,57,54]

    control_tray = [47]
    tray_5_ai = [40, 41, 42, 43, 273, 276, 275, 274]

    # tray_11_ai = [91]
    start_date = "2021-08-10 00:00:00"
    main(tray_5_ai, control_tray,start_date)
