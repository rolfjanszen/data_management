
import argparse
import sys


import cv2
from os import listdir, path, mkdir,makedirs
from os.path import isfile, join, expanduser


import numpy as np

from tools.general.helpers import get_file_list


def dissasemble_video(cap, max_frames, file_base_path):
    counter = 0

    while cap.isOpened() and counter < max_frames:
        ret, frame = cap.read()
        # frame=cv2.resize(frame,(0,0),fx=0.5,fy=0.5)
        if ret:
            save_name = 'frame_{:04}.png'.format(counter)
            save_path = join(file_base_path, save_name)
            cv2.imwrite(save_path, frame)
            counter += 1
        else:
            break


def process_video(cap, file_base_name, crop_frame, max_frames,get_last,new_scale):

    counter = 0

    increment = 2#int(cap.get(cv2.CAP_PROP_FRAME_COUNT)/max_frames)
    if increment < 1:
        increment =1

    done_frames = 3# increment * max_frames


    while cap.isOpened() and counter < done_frames:

        if max_frames == 1 and get_last:
            cap.set(1, cap.get(cv2.CAP_PROP_FRAME_COUNT) - 1)
            ret, frame = cap.read()
        elif increment <2:
            print("Warning, incrment is 1")
            ret, frame = cap.read()
        else:
           for _ in range(5):
                ret, frame = cap.read()
        if not ret:
            return
        # cv2.imshow('frame',frame)
        # print(frame.shape)
        # cv2.waitKey()
        if crop_frame:
            frame_crop = frame[:, 420:-420]

            half_width = int(frame_crop.shape[0] / 2)
            half_height = int(frame_crop.shape[1] / 2)

            quarter_frames = [frame_crop[:half_width, :half_height],
                              frame_crop[half_width:, :half_height],
                              frame_crop[:half_width, half_height:],
                              frame_crop[half_width:, half_height:]]
        else:
            frame_crop = frame
            quarter_frames = [frame]

        half_width = int(frame_crop.shape[0] / 2)
        half_height = int(frame_crop.shape[1] / 2)
        if max_frames > 1:
            file_count_name = file_base_name+ '_' + str(counter) + '_0'
        else:
            file_count_name = file_base_name
        print('writing for ', file_count_name)

        for frame in quarter_frames:

            save_name = file_count_name + '.png'
            cv2.imwrite(save_name, frame)
            print('saved as ', save_name)
            if crop_frame:
                for angle in [90, 180, 270]:
                    center = tuple(np.array([half_height, half_width]) / 2)
                    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
                    frame = cv2.warpAffine(
                        frame, rot_mat, (half_height, half_width))
                    save_name = file_count_name + str(angle) + '.png'
            if new_scale != 1.0:
                frame = cv2.resize(frame, (0, 0), fx=new_scale, fy=new_scale)
            cv2.imwrite(save_name, frame)
            if max_frames < 2:
                cap.release()

        counter += 1



def run(path_adjective, crop_frame, max_frames, file_path, new_scale, get_last):

    #     file_paths = ['size_classes/class0','size_classes/class1', 'size_classes/class2','size_classes/class3']
    #     file_paths = ['size_classes/class5']#,'size_classes/class4']

    file_paths = [expanduser(file_path)]
    for file_path in file_paths:
        files = get_file_list(file_path,['avi','mkv','kv,'])

        save_dir = file_path
        if path_adjective[0] == '/':
            save_dir = path_adjective
        else:
            save_dir = join(save_dir,path_adjective + '_img')
        print('save_dir',save_dir)
        if not path.isdir(save_dir):
            makedirs(save_dir)

        for file in files:
            file_name= join(file_path, file)
            try:
                cap = cv2.VideoCapture(file_name)
            except:
                print('not a video format', file_name)
                break
            save_path = join(save_dir, file[:-4])


            if max_frames > 10:
                dissasemble_video(cap, max_frames, save_dir)
            else:
                process_video(cap, save_path, crop_frame, max_frames,get_last,new_scale)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_path', help="Add string for path to save files",
                        type=str, required=False, default='images')
    parser.add_argument('--file_path', help="File path  to source ",
                        type=str, required=True)
    parser.add_argument('--crop_frame', help="Crop frame ", type=bool,
                        required=False, default=False)
    parser.add_argument('--max_frames', help="Maximum number of frames per clip ",
                        type=int, required=False, default=1)

    parser.add_argument('--new_scale', help="rescale value image ",
                        type=float, required=False, default=1.0)

    parser.add_argument('--last_frame', help="Get the last frame",
                        type=str, required=False, default=False)


    args = parser.parse_args()
    print('crop frm: ', args.crop_frame)
    run(args.save_path, args.crop_frame, args.max_frames, args.file_path, args.new_scale, args.last_frame)
