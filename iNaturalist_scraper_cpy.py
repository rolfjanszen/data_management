import pandas as pd
import numpy as np
from urllib import request
from os.path import expanduser, join, isdir
from  os import makedirs
import shutil
import time

"""
Scrapes iNaturalis to get the image for labelled data
This script takes a csv file downloaded from the iNaturalist website
with url to an image with taxonomi data.
The taxonomi serves as class label
Output:
a txt file with the image name and class number
A folder with all the downloaded images
also a pickle file with a class dictionairy
in case to recover from a crash.
"""

iNaturalist_csv = expanduser('~/Documents/observations-57842.csv')

df = pd.read_csv(iNaturalist_csv)
classes = dict()
last_class_nr = 0
file_path = expanduser('~/Labeled/classification/iNaturalist')

if isdir(file_path):
    shutil.rmtree(file_path) 

im_save_path  = join(file_path,'images')
if not isdir(im_save_path):
    makedirs(im_save_path)

if not isdir(file_path):
    makedirs(file_path)
        
label_file_name = 'inaturalist_set.txt'
label_file_path = join(file_path, label_file_name)
# file = open(label_file_path, 'a+')
counter = 0
df= df.dropna(subset=['taxon_superfamily_name','image_url'])
for index, row in df[20:].iterrows():
    file = open(label_file_path, 'a')
    class_name = row.taxon_superfamily_name

    url = row.image_url
    url_parts = url.split('/')
    image_name = url_parts[-2] + '_' + url_parts[-1].split('?')[0]

    print(class_name)
    if class_name in classes:
        class_nr = classes[class_name]
    else:
        classes[class_name] = last_class_nr
        class_nr = last_class_nr
        last_class_nr += 1

    try:
        im_path = join(im_save_path,image_name)
        f = open(im_path, 'wb')
        f.write(request.urlopen(url).read())
    
        new_line = image_name + ' ' + str(class_nr)+'\n'
        file.write(new_line)
        file.close()
    
        print('saved file ',index, new_line)
    except:
        print('not able to retrieve file: ',index,image_name)
