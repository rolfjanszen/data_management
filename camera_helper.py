import cv2
import math
import argparse
from sys import argv

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cam_device', help="Camera device",
                        type=str, required=True)

    args = parser.parse_args(args=argv[1:])

    cam_device = args.cam_device

    # cam_device = 0
    cap = cv2.VideoCapture(cam_device)
    if not cap.isOpened():
        print("could not open camera")
        exit(0)
    ret, frame = cap.read()
    key = -1

    re_scale = 0.4

    ret, frame = cap.read()
    mid_y, mid_x = frame.shape[:2]
    mid_y, mid_x = int(mid_y / 2), int(mid_x / 2)
    crop_dimension =[1800,1600]
    while True:
        cam_device = "rtsp://admin:C0ntrolCam@192.168.0.98"
        ret, frame = cap.read()

        if mid_x <= int(crop_dimension[0] / 2):
            mid_x = int(crop_dimension[0] / 2)+2
        if mid_y <= int(crop_dimension[0] / 2):
            mid_y = int(crop_dimension[0] / 2)+2

        pnt1 = (int(mid_x - crop_dimension[0] / 2),
                int(mid_y - crop_dimension[1] / 2))

        pnt2 = (int(mid_x + crop_dimension[0] / 2),
                int(mid_y + crop_dimension[1] / 2))
        if key == 111:  # o
            re_scale -= 0.1
            ret, frame = cap.read()
        elif key == 112:  # p
            re_scale += 0.1
            ret, frame = cap.read()
        elif key == 100:  # d
            ret, frame = cap.read()
            mid_x += 6
        elif key == 97:  # a
            ret, frame = cap.read()
            mid_x -= 26
        elif key == 119:  # w
            ret, frame = cap.read()
            mid_y -= 26
        elif key == 115:  # s
            ret, frame = cap.read()
            mid_y += 26
        elif key == 110:  # n
            ret, frame = cap.read()
            frame = cv2.resize(frame, (0, 0), fx=re_scale, fy=re_scale)
            mid_y, mid_x = frame.shape[:2]
            mid_y, mid_x = int(mid_y / 2), int(mid_x / 2)
        elif key == 102: #f
            cv2.waitKey()
        elif key ==27:
            break
        if re_scale < 0.25:
            re_scale = 0.25


        re_scale = math.ceil(re_scale * 100) / 100

        frame = cv2.resize(frame, (0, 0), fx=re_scale, fy=re_scale)
        crp = [pnt1[1], pnt2[1], pnt1[0], pnt2[0]]
        # frame = frame[crp[0]:crp[1], crp[2]:crp[3],:]
        # print(re_scale, crp)
        # print(frame.shape)
        if frame.shape[0] < 10:
            key= 110
        else:
            cv2.imshow('frame', frame)
            key = cv2.waitKey(100)



