import cv2
from os import listdir, path, mkdir
from os.path import isfile, join, expanduser
import sys
import argparse
tool_box_path = expanduser('~/scripts/tools')
sys.path.append(tool_box_path)

from helpers import  get_file_list

class Clips2Video:
    """Takes a set of avi clips or images and concatenates them into a single video."""
    
    out = None

    def __init__(self, save_dir,  file_path, file_type, save_name = 'result.avi'):

        self.files = get_file_list(file_path, file_type)
        if not path.isdir(save_dir):
            mkdir(save_dir)
     
        self.file_path = file_path
        self.video_title = join(save_dir, save_name)
        out = None
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.total_files = len(self.files)
        if self.total_files < 1:
            print('ERROR: no files of that type',file_type ,' found in ', file_path)
            exit(0)
        
        if input_type == 'avi':    
            cap = cv2.VideoCapture(join(file_path, self.files[0]))
            ret, frame = cap.read()
        else:
            frame = cv2.imread(join(self.file_path,self.files[100]))
            ret = True

        if not ret:
            print('ERROR: no frame in ', self.files[0])
            exit(0)

        if out is None:
            frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            h, w = frame.shape[:2]

            self.out = cv2.VideoWriter(self.video_title, fourcc, 5, (w, h))


    def create_video_from_images(self):
        nr_files = len(self.files)
        for i, file in enumerate(self.files):
            try:
                frame = cv2.imread(join(self.file_path,file))
                frame = cv2.resize(frame,(0,0),fx=0.25,fy=0.25)
                print('reading ',i,'of ',nr_files,file,frame.shape)
                if frame is None:
                    print('something went wrong reading image', file,' make sure this is an image and not an video')
                    break
    
                self.out.write(frame)
                cv2.imshow('frame', frame)
                key = cv2.waitKey(20)
                if key ==27:
                    break
            except:
                print('Erroneous file, skipping')
        # if self.out is not None:
        self.out.release()

    def create_video_from_videos(self):

        print('reading in create_video_from_videos',self.file_path)
        for i, file in enumerate(self.files):
            
            cap = cv2.VideoCapture(join(self.file_path, file))
            print('reading file ', i, ' of ', self.total_files, ' :', file)
            ret, frame = cap.read()
            if not ret:
                print('something went wrong reading video', file,' make sure this is an video and not an image')
            while cap.isOpened():
                self.out.write(frame)
                ret, frame = cap.read()

                if not ret:
                    break

            cap.release()

        if self.out is not None:
            self.out.release()
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--file_path', help="File path to the files to be concatenaed into a video",
                         type=str,required=True)
    parser.add_argument('--file_type',type=str,required = True,
                        default ='avi', help='type of (last 3 letters) input files, e.g. avi, jpg, png... can be a longer name.'
                        'I.e. segm_rslt.png to get certain images from a file.')
    parser.add_argument('--save_path', help="Add string for path to save files",
                         type=str, required=False, default = expanduser('~/'))
    parser.add_argument('--save_name', help="Add string for path to save files",
                         type=str, required=False, default = 'result.avi')      
    args = parser.parse_args()

    file_path= expanduser(args.file_path)
    input_type = args.file_type
    converter = Clips2Video(args.save_path, file_path, input_type,args.save_name)

    print('input_type',input_type)
    if input_type == 'avi':
        converter.create_video_from_videos()
    else:
        converter.create_video_from_images()
    
    print('File saved as ',converter.video_title)

