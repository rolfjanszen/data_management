from os.path import isdir, join, expanduser
from os import listdir, makedirs
import shutil

CONTAINER_BOX ='container_box'
def fix_container_box_names(base_path):
    folder_names = listdir(base_path)
    for folder_name in folder_names:
        full_path = join(base_path,folder_name)
        if not isdir(join(full_path,'recordings')):
            continue
        if CONTAINER_BOX != folder_name[:13]:
            folder_split = folder_name.split('/')
            box_name = folder_split[-1]
            new_folder_name = CONTAINER_BOX+box_name
            print('new_folder_name',new_folder_name)
            back_up_path = expanduser('~/Data/corrected_pathsx1-4')
            # new_path = join(base_path,new_folder_name)
            new_path = join(back_up_path,new_folder_name)
            # if not isdir(new_path):
            #     makedirs(new_path)
            shutil.copytree(full_path, new_path)
        #   break


if __name__ == "__main__":
    # container_path = '/media/overmind/69C33E411B8A4C3E/x1-4'
    fix_container_box_names(container_path)