import datetime
import os
from os import listdir, path, mkdir
from os.path import isfile, join, expanduser
import time

def get_title(base_folder,format = '.avi'):
    """
    Create video title based on date and time
    :return: Title for the video
    """
    now = datetime.datetime.now()
    date_now = now.date().isoformat()
    time_now = now.time().isoformat()[:8]
    time_now = time_now.replace(':','_')
    video_title = os.path.join(base_folder, date_now + '_' + time_now + format)
    return video_title

def get_file_list( file_path, format = 'avi'):
    """Create a list of files in a folder that have a certain
    format appended to their name (can be anything)."""
    file_list = [f for f in listdir(file_path) if isfile(
        join(file_path, f)) and f[-3:] == format]
    file_list.sort()

    return file_list


