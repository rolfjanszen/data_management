import cv2
from os import mkdir
from os import listdir
import random
from os.path import isfile, join, expanduser, isdir
import sys
import numpy as np
import argparse

"""
Create labels for object detector by taking burst videos.
Labelled images are augmented (rotated, width/ height shifted)
to increase the variation of labelled data.
HOWTO: When creating a bounding box, click top right before bottom left of
the black larvae.
"""

# img = None


class CoordinateStore:
    key = -1
    def __init__(self, window_name):
        self.win_name = window_name
        self.points = []
        self.store_points = []
        
        
    def select_point(self, event, x, y, flags, param):

        nr_points = len(self.store_points)
        self.draw_image = self.image.copy()

        if  nr_points > 0:
            cv2.rectangle(self.draw_image, self.store_points[0], (x,y), (255, 0, 0), 2)            
            
        if event == cv2.EVENT_FLAG_LBUTTON:
            cv2.circle(self.draw_image, (x, y), 3, (255, 0, 0), -1)
            self.store_points.append((x, y))
            print(x, y)
            if len(self.store_points) > 1:
                self.points.append(self.store_points)
                self.store_points = []


    
    
    def user_define_boundary(self,img):
        """
        Using the mouse event, get user input on where the 
        bounding boxes should be in the image.
        """
        self.store_points = []
        self.points = []
        
        self.image = img.copy()
        cv2.imshow(self.win_name,self.image)
        cv2.waitKey(60)
        self.draw_image = img.copy()
        while True:
            cv2.namedWindow(self.win_name)
            cv2.setMouseCallback(self.win_name, self.select_point)
                            
            cv2.imshow(self.win_name,self.draw_image)
            self.key = cv2.waitKey(60)
            if self.key == 13 and len(self.points):  # ENTER
                break
            
            if self.key == 99:  # c
                if len(self.points) > 0:
                    self.points.pop()
                self. store_points = []   
            if self.key == 27:  # ESC
                self.points = []
                break
            
            for point  in self.points:
                cv2.rectangle(self.draw_image, point[0],point[1], (255, 0, 0), 2)
            
    
        cv2.destroyWindow(self.win_name)
        return self.points

def get_images(file_path, file_format = '.avi'):

    black_larve_list = [join(file_path, f) for f in listdir(file_path)
                        if isfile(join(file_path, f)) and f[-4:] == file_format]

    black_larve_list.sort()
    return black_larve_list


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH)), M


def check_point_order(points):

    is_first_right = points[0][0] < points[1][0]
    is_first_top = points[0][1] < points[1][1]

    return is_first_right and is_first_top



def augment_image_coord(obj_width, obj_height, mid_point,  full_dim, im_height, im_width):
    """
    Find a random number by which to crop the image around the annotated objects.
    Make sure the number by which to shift the image coordinates to their cropped 
    coordinates doesn't end up outside the original image.
    """
    size_buff = 20
    width_range = full_dim - obj_width - size_buff
    height_range = full_dim - obj_height - size_buff

    print(height_range, width_range)

    if width_range < 1:
        width_range = 1

    if height_range < 1:
        height_range = 1

    width_shift = np.random.randint(0, width_range)
    width_shift -= int(width_range / 2)
    height_shift = np.random.randint(0, height_range)
    height_shift -= int(height_range / 2)

    half_dim = int(full_dim / 2)

    # Range for cropped area from the rotated image.
    width_range = [mid_point[0] + width_shift - half_dim,
                   mid_point[0] + width_shift + half_dim]

    height_range = [mid_point[1] + height_shift - half_dim,
                    mid_point[1] + height_shift + half_dim]

    # Make sure the cropped region will be inside the bounds of the rotated
    # image.
    if width_range[0] < 0:
        width_shift -= width_range[0]

    if width_range[1] > im_width:
        width_shift -= width_range[1] - im_width

    width_range = [mid_point[0] + width_shift - half_dim,
                   mid_point[0] + width_shift + half_dim]

    if height_range[0] < 0:
        height_shift -= height_range[0]

    if height_range[1] > im_height:
        height_shift -= height_range[1] - im_height

    height_range = [mid_point[1] + height_shift - half_dim,
                    mid_point[1] + height_shift + half_dim]

    return width_range, height_range


def create_random_rotation(img, points):
    """
    Rotate the image and bounding box coordinates by
    0, 90, 180 or 279 degrees.
    """
    rand_rot = random.sample([0, 90, 180, 270], 1)[0]

    rotated, M = rotate_bound(img, rand_rot)

    new_points = []

    for i, point in enumerate(points):
        new_pt = []
        for pt in point:

            point_l = list(pt)
            point_l.append(1)

            new_point = M.dot(point_l)
            new_pt.append(new_point)
        new_points.append(new_pt)

    return rotated, new_points


def get_object_dimensions(points, h_range, w_range):
    """
    From all (transformed) bounding boxes, get the
    object width, height and midpoints of all 
    these bounding boxes and correct them for the
    shift in image coordinate due to cropping.
    """
    objects = []

    for point in points:

        obj_width = abs(point[0][0] - point[1][0])
        obj_height = abs(point[0][1] - point[1][1])

        mid_point = [int((point[0][0] + point[1][0]) / 2 - h_range[0]),
                     int((point[0][1] + point[1][1]) / 2 - w_range[0])]

        new_obj = {'w': obj_width, 'h': obj_height, 'm': mid_point}
        objects.append(new_obj)

    return objects


def calculate_mid_point(points):
    """
    Get the average centre of all bounding boxes
    """
    w_middle = 0
    h_middle = 0

    for point in points:

        w_middle += point[0][0] + point[1][0]
        h_middle += point[0][1] + point[1][1]

    nr_points = len(points) * 2
    mid_point = [int(w_middle / nr_points), int(h_middle / nr_points)]

    return mid_point


def get_width_height_crop(points):
    """
    Get the full length between the outer points of the
    bounding boxes. The width and height returned is the 
    minimum width and height needed for an image to  hold
    all bounding boxes.
    """

    heights = []
    widths = []

    for point in points:
        obj_width = abs(point[0][0] - point[1][0])
        obj_height = abs(point[0][1] - point[1][1])

        widths.append(point[0][0])
        widths.append(point[1][0])

        heights.append(point[0][1])
        heights.append(point[1][1])

    width = int(max(widths) - min(widths))
    height = int(max(heights) - min(heights))

    return width, height


def create_labels_images(img, points, full_dim):
    """
    Taking the points of the bounding boxes.
    Randomly augment the image and bounding box coordinates
    by rotating and shift/ cropping the image to create a 
    large variety of images from frames of one clip.
    """
    img_rot, rot_points = create_random_rotation(img, points)
    center_all_points = calculate_mid_point(rot_points)
#     print(mid)

    for rot_point in rot_points:

        mid_point = [int((rot_point[0][0] + rot_point[1][0]) / 2),
                     int((rot_point[0][1] + rot_point[1][1]) / 2)]
        print('mid_point', mid_point)

    crop_width, crop_height = get_width_height_crop(rot_points)

    # Make sure the final image is big enough for all bounding boxes.
    if crop_width > full_dim:
        full_dim = crop_width
    if crop_height > full_dim:
        full_dim = crop_height

    im_height = img_rot.shape[0]
    im_width = img_rot.shape[1]

    width_range, height_range = augment_image_coord(
        crop_width, crop_height, center_all_points, full_dim, im_height, im_width)

    cropped_image = img_rot[height_range[0]:height_range[1],
                            width_range[0]:width_range[1]]

    object_labels = get_object_dimensions(
        rot_points, width_range, height_range)

    rect_crop = cropped_image.copy()
    annotation_string = ''

    for object_label in object_labels:

        top_right = (int(object_label['m'][0] - object_label['w'] / 2),
                     int(object_label['m'][1] - object_label['h'] / 2))
        bot_left = (int(object_label['m'][0] + object_label['w'] / 2),
                    int(object_label['m'][1] + object_label['h'] / 2))
        print(top_right, bot_left)
        cv2.rectangle(rect_crop, top_right, bot_left, (255, 0, 0), 2)
        cv2.imshow('check bound', rect_crop)
        key_ok = cv2.waitKey(20)

        annotation_string += '0 ' + str(object_label['m'][0] / full_dim) + \
            ' ' + str(object_label['m'][1] / full_dim) + \
            ' ' + str(object_label['w'] / full_dim) + \
            ' ' + str(object_label['h'] / full_dim) + '\n'
    print(annotation_string)
    return cropped_image, annotation_string, key_ok


def annotate_data(file_path, full_dim, scale):
    """
    Main function for creating bounding boxes.
    """
    file_path = expanduser(file_path)
    standard_name = 'custome_set_'
    print('file_path',file_path)
    files = get_images(file_path)

    base_dir = expanduser('~/Documents/yolo/')

    label_save_dir = join(base_dir, 'train_labels')
    image_save_dir = join(base_dir, 'train_images_neg')
    meta_data_file_name = join(base_dir, '5k.txt')
    points_collect = CoordinateStore('select points')
    saved_files = get_images(image_save_dir,'.jpg')
        
    start_number = len(saved_files)
    
    if not isdir(base_dir):
        mkdir(base_dir)

    if not isdir(label_save_dir):
        mkdir(label_save_dir)

    if not isdir(label_save_dir):
        mkdir(label_save_dir)

    if not isdir(image_save_dir):
        mkdir(image_save_dir)

    counter = start_number
    frames_past = 0

    for file in files[:50]:

        cap = cv2.VideoCapture(file)
        ret, img = cap.read()

        if not ret:
            break

        img = cv2.resize(img, (0, 0), fx=scale, fy=scale)
        print(img.shape)
        points = points_collect.user_define_boundary(img)
#         points = [[(595, 324), (663, 387)],
#                   [(330, 240), (405, 317)]]
        while cap.isOpened():

            ret, img = cap.read()

            if not ret or len(points) < 1:
                break

            counter += 1

            frame = cv2.resize(img, (0, 0), fx=scale, fy=scale)

            if len(points) > 0:

                data_name = standard_name + str(counter)

                label_save_name = join(image_save_dir, data_name + '.txt')
                image_save_name = join(image_save_dir, data_name + '.jpg')
                
                meta_data_file = open(meta_data_file_name, 'a')
                meta_data_file.write(image_save_name + '\n')
                meta_data_file.close()

                label_file = open(label_save_name, 'a')
                cropped_image, annotation, key_ok = create_labels_images(
                    frame, points, full_dim)
            
                label_file.write(annotation)
                label_file.close()

                """Reset bounding box."""
                
                key = cv2.waitKey()
                if key_ok == 'n' or key == 99:
                    cv2.destroyAllWindows()
                    points = points_collect.user_define_boundary(frame)
                    key_ok = 0
                if key == 27:
                    break

                cv2.imwrite(image_save_name, cropped_image)
                print('file saved to ', image_save_name, cropped_image.shape)


if __name__ == '__main__':
    path = expanduser('~/rec/record_data/videos_box1')

    parser = argparse.ArgumentParser()
    parser.add_argument('--file_path', help="Add string for path to save files",
                         type=str, required=False, default=path)
    parser.add_argument('--crop_size', help="Square pixel size cropped annotated image",
                     type=int, required=False, default=128)
    parser.add_argument('--re_scale', help="Re-scaling original image",
                     type=float, required=False, default=0.5)
    args = parser.parse_args()

    annotate_data(args.file_path, args.crop_size, args.re_scale)
