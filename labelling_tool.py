import sys

from os.path import isfile, expanduser, join, isdir
from os import listdir
import pandas as pd
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel,QWidget, QComboBox, QMainWindow, QCheckBox, QPushButton


def get_file_list( file_path, format = 'avi'):
    """Create a list of files in a folder that have a certain
    format appended to their name (can be anything)."""
    if not isdir(file_path):
        Exception("{} is not a direcory".format(file_path))
    len_frmt= len(format)
    file_list = [f for f in listdir(file_path) if isfile(
        join(file_path, f)) and f[-len_frmt:] == format]
    file_list.sort()

    return file_list



class MainWindow(QMainWindow):
    switch_window = QtCore.pyqtSignal(str)
    labeled_csv = 'labels.csv'
    csv_file = None
    container_name = 'test_container'
    tray_name = 'test_tray'
    label_columns = ['container','tray','image nr','feed more','diseased','dehydrated','mounding','comments','labelled']
    counter = 0

    def __init__(self,*args, **kwargs):

        super(MainWindow, self).__init__(*args, **kwargs)

        self.im_path =expanduser('~/Data/big_box3/measurement_frames')
        self.title = 'Image labeler'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        main_layout = QtWidgets.QVBoxLayout()
        file_layout= QtWidgets.QHBoxLayout()
        layout = QtWidgets.QGridLayout()
        cntrl_layout = QtWidgets.QHBoxLayout()

        file_path_instructions = QLabel()
        file_path_instructions.setText("Path to images:")
        self.file_path = QtWidgets.QLineEdit()
        self.file_set_button = QPushButton()
        self.file_set_button.setText("Start")
        self.file_set_button.clicked.connect(lambda: self.start_labelling())

        file_layout.addWidget(file_path_instructions)
        file_layout.addWidget(self.file_path)
        file_layout.addWidget(self.file_set_button)

        selection_layout = self.init_selections()
        self.im_display = QLabel(self)
        comment_layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.im_display)
        comment_l= QLabel()
        comment_l.setText("Comments:")
        self.line_edit = QtWidgets.QLineEdit()
        comment_layout.addWidget(comment_l)
        comment_layout.addWidget(self.line_edit)


        self.button = QtWidgets.QPushButton('Save/Next image')
        self.back_button = QtWidgets.QPushButton('< prev ')
        self.fwd_button = QtWidgets.QPushButton('fwd > ')
        self.button.clicked.connect(self.save_next)
        self.back_button.clicked.connect(self.load_prev)
        self.fwd_button.clicked.connect(self.load_next)

        cntrl_layout.addWidget(self.button,0)
        cntrl_layout.addWidget(self.back_button,0)
        cntrl_layout.addWidget(self.fwd_button,0)
        main_layout.addLayout(file_layout)
        main_layout.addLayout(layout)
        main_layout.addLayout(selection_layout)
        main_layout.addLayout(comment_layout)
        main_layout.addLayout(cntrl_layout)

        widget = QWidget()
        widget.setLayout(main_layout)
        self.setCentralWidget(widget)
        # self.load_image()

    def init_selections(self):

        feed_more_l = QLabel()
        diseased_l = QLabel()
        dehydrated_l = QLabel()
        mounding_l = QLabel()

        self.feed_more_c = QCheckBox()
        self.diseased_c = QCheckBox()
        self.dehydrated_c = QCheckBox()
        self.mounding_c = QCheckBox()

        feed_more_l.setText("Feed more")
        diseased_l.setText("Diseased larvae")
        dehydrated_l.setText("Dehydrated")
        mounding_l.setText("Mounding")
        selection_layout = QtWidgets.QHBoxLayout()
        selection_layout.addWidget(feed_more_l)
        selection_layout.addWidget(self.feed_more_c )
        selection_layout.addWidget(diseased_l)
        selection_layout.addWidget(self.diseased_c)
        selection_layout.addWidget(dehydrated_l)
        selection_layout.addWidget(self.dehydrated_c)
        selection_layout.addWidget(mounding_l)
        selection_layout.addWidget(self.mounding_c)

        return selection_layout

    def start_labelling(self):
        self.im_path = self.file_path.text()

        if not isdir(self.im_path):
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage('Folder {} not found. Please set a correct directory'.format(self.im_path))
            return
        self.get_image_list()
        self.load_image()

    def load_image(self):

        im = self.to_label[self.counter]
        im = join(self.im_path, im)
        pixmap = QPixmap(im)
        pixmap = pixmap.scaled(pixmap.width()/3, pixmap.height()/3)
        # self.resize()
        self.im_display.setPixmap(pixmap)
        self.im_displayed = im

    def empty_fields(self):
        self.feed_more_c.setChecked(False)
        self.mounding_c.setChecked(False)
        self.dehydrated_c.setChecked(False)
        self.diseased_c.setChecked(False)
        self.line_edit.setText('')

    def save_next(self):
        feed_m = self.feed_more_c.checkState()
        mounding = self.mounding_c.checkState()
        dehydrate = self.dehydrated_c.checkState()
        disease = self.diseased_c.checkState()
        commenting = self.line_edit.text()
        filled_in = [self.container_name,self.tray_name,self.im_displayed,feed_m, dehydrate, disease, mounding, commenting,True]
        print(feed_m, dehydrate, disease, mounding)
        new_data = pd.Series(data=filled_in,index=self.df.columns)
        if self.counter not in self.df.index:
            new_data.name = self.counter
            self.df = self.df.append(new_data, ignore_index=True)

        else:
            self.df.iloc[self.counter] = new_data
        self.df.to_csv(self.labeled_csv, index=False)
        self.load_next()


    def load_prev(self):
        if self.counter == 0:
            return

        self.counter -= 1
        self.load_from_data()
        self.load_image()

    def load_next(self):
        if self.counter == len(self.to_label) -1:
            return

        self.counter += 1

        self.load_from_data()
        self.load_image()

    def load_from_data(self):
        if self.counter not in self.df.index:
            self.empty_fields()
            return

        entry = self.df.iloc[self.counter]

        self.feed_more_c.setChecked(int(entry[3] )> 0)
        self.mounding_c.setChecked(int(entry[6]) > 0)
        self.dehydrated_c.setChecked(int(entry[4]) > 0)
        self.diseased_c.setChecked(int(entry[5]) > 0)
        fill_text =entry[7]
        if pd.isna(fill_text):
            fill_text = ''
        self.line_edit.setText(fill_text)

    def init_data(self):
        if isfile(self.labeled_csv):
            # self.csv_file = open(self.labeled_csv)
            self.df = pd.read_csv(self.labeled_csv)
            self.df.reset_index()
        else:
            self.df = pd.DataFrame(columns=self.label_columns)

    def get_image_list(self):
        self.init_data()
        images_labeled = self.df['image nr'].to_list()
        self.counter = len(images_labeled)
        images = get_file_list(self.im_path,'png')

        self.to_label =images_labeled + [im for im in images if im not in images_labeled ]


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
