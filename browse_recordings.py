import abc
import sys
import time
from os import listdir, name
from os.path import expanduser, join, isdir
from typing import Union

import cv2
import numpy as np
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QThread, QObject, pyqtSignal
from PyQt5.QtGui import QFont
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QLabel, QLCDNumber, QMainWindow, QPushButton, QWidget, QVBoxLayout

SLASH = '/' if name == 'posix' else "\\"


class BaseBrowser(QMainWindow):
    save_image_button: Union[QPushButton, QPushButton]
    switch_window = QtCore.pyqtSignal(str)
    container_name = 'test_container'
    tray_name = 'test_tray'
    counter = 0

    def __init__(self, title, *args, **kwargs):
        super(BaseBrowser, self).__init__(*args, **kwargs)

        # self.im_path = expanduser('/media/overmind/SWAPDISK2')
        self.title = title
        self.left = 10
        self.top = 10
        self.width = 440
        self.height = 280
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.file_layout = QtWidgets.QHBoxLayout()
        self.layout = QtWidgets.QHBoxLayout()
        self.cntrl_layout = QtWidgets.QHBoxLayout()
        self.information_layout = QtWidgets.QHBoxLayout()
        self.file_set_button = QPushButton()
        self.file_set_button.setText("Start")
        self.file_set_button.clicked.connect(self.start_browsing)
        file_path_instructions = QLabel()
        file_path_instructions.setText("Path to images:")
        self.file_path = QtWidgets.QLineEdit()
        self.file_path_shown = QtWidgets.QLabel()

        self.file_layout.addWidget(file_path_instructions)
        self.file_layout.addWidget(self.file_path)
        self.file_layout.addWidget(self.file_set_button)

        self.information_layout.addWidget(self.file_path_shown)
        self.im_display = QLabel(self)
        self.layout.addWidget(self.im_display)
        self.line_edit = QtWidgets.QLineEdit()

        # self.button = QtWidgets.QPushButton('Save/Next image')
        self.back_button = QtWidgets.QPushButton('< prev ')
        self.fwd_button = QtWidgets.QPushButton('next > ')
        self.save_image_button = QtWidgets.QPushButton('save/ next ')

        self.file_layout.addWidget(self.file_set_button)
        self.back_button.clicked.connect(self.load_prev)
        self.fwd_button.clicked.connect(self.load_next)

        # self.cntrl_layout.addWidget(self.button, 0)
        self.cntrl_layout.addWidget(self.back_button, 0)
        self.cntrl_layout.addWidget(self.fwd_button, 0)
        self.cntrl_layout.addWidget(self.save_image_button, 0)

    def start_browsing(self):
        self.im_path = self.file_path.text()
        # full_path = join(self.im_path,self.image_sub_folder)
        print("full_path", self.im_path)
        if not isdir(self.im_path):
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage(
                'Folder {} not found. Please set a correct directory'.format(self.im_path))
            return
        self.get_image_list(self.im_path)
        self.load_image()

    @abc.abstractmethod
    def load_next(self):
        Exception("ERROR not implemented")

    @abc.abstractmethod
    def load_prev(self):
        Exception("ERROR not implemented")


class VideoPlayer(QObject):
    disp_scale = 0.6
    keep_playing = True
    finished = pyqtSignal()
    vid_px_map = pyqtSignal(QPixmap)
    im = ''
    show_black = False
    im_message = ''

    def __init__(self):
        super(VideoPlayer, self).__init__()

    def gen_qpix_from_frame(self, frame):

        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # re_scale = self.size_slider.value()/10
        frame = cv2.resize(frame, (0, 0), fx=self.disp_scale, fy=self.disp_scale)
        height, width = frame.shape[:2]
        bytesPerLine = 3 * width
        q_img = QImage(frame.data, width, height, bytesPerLine, QImage.Format_RGB888)
        return QPixmap(q_img)

    def run(self):
        if not len(self.im):
            return
        current_im = self.im
        cap = cv2.VideoCapture(self.im)
        if not cap.isOpened():
            print('could not open video')
            return
        # self.im = ''
        while self.keep_playing:

            if self.show_black:
                frame = np.zeros((3000,4000,3),dtype=np.uint8)
                frame = cv2.putText(frame, "No video for date", (100, 500), cv2.FONT_HERSHEY_SIMPLEX, 10,
                                    (255, 255, 255), 4)
            elif self.im is not None and len(self.im) > 2:
                ret, frame = cap.read()

            if len(self.im_message):
                frame = cv2.putText(frame, self.im_message, (200, 500), cv2.FONT_HERSHEY_SIMPLEX, 10,
                                    (0, 251, 255), 20)
            if not ret or current_im != self.im:
                cap.release()
                current_im = self.im
                if len(self.im):
                    cap = cv2.VideoCapture(self.im)
                    ret, frame = cap.read()
            if ret:
                frame= cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                pixmap = self.gen_qpix_from_frame(frame)
                self.vid_px_map.emit(pixmap.scaled(pixmap.width() / 3, pixmap.height() / 3))
            time.sleep(0.2)


class FeedSessDisplay(QVBoxLayout):
    vid_is_playing = False

    def __init__(self, display_name_: str, disp_scale_=0.6):
        super(FeedSessDisplay, self).__init__()

        self.disp_scale = disp_scale_
        display_information = QtWidgets.QHBoxLayout()
        vid_date_label = QLabel("video date")
        self.vid_date_display = QLabel()
        display_name = QLabel(display_name_)
        display_name.setFont(QFont('Arial', 20))
        display_information.addWidget(display_name)
        display_information.addWidget(vid_date_label)
        display_information.addWidget(self.vid_date_display)

        self.fc_signal = QLCDNumber()
        self.fc_signal.setSegmentStyle(QLCDNumber.Flat)
        self.nn_signal = QLCDNumber()
        self.nn_signal.setSegmentStyle(QLCDNumber.Flat)
        self.feed_frac = QLCDNumber()
        self.feed_frac.setSegmentStyle(QLCDNumber.Flat)
        self.feed_mod = QLCDNumber()
        self.feed_mod.setSegmentStyle(QLCDNumber.Flat)
        self.feed_pumped = QLCDNumber()
        self.feed_pumped.setSegmentStyle(QLCDNumber.Flat)
        self.tray_age = QLCDNumber()
        self.tray_age.setSegmentStyle(QLCDNumber.Flat)
        self.cumm_food = QLCDNumber()
        self.cumm_food.setSegmentStyle(QLCDNumber.Flat)

        fc_sig_lbl = QLabel("Segmentation pred.:")
        nn_signal_lbl = QLabel("Modifier pred.:")
        feed_mod_lbl = QLabel("Modifier (%):")
        feed_frac_lbl = QLabel("Feed detect:")
        tray_age_lbl = QLabel("Age (days):")
        food_pumped = QLabel("Food given (ml):")

        spacing = 10
        self.results_layout = QtWidgets.QVBoxLayout()

        tray_information_layout = QtWidgets.QHBoxLayout()
        tray_information_layout.addWidget(tray_age_lbl)
        tray_information_layout.addWidget(self.tray_age)
        tray_information_layout.insertSpacing(-1, spacing)
        tray_information_layout.addWidget(food_pumped)
        tray_information_layout.addWidget(self.feed_pumped)
        tray_information_layout.insertSpacing(-1, 60)

        feed_information_layout = QtWidgets.QHBoxLayout()
        feed_information_layout.addWidget(fc_sig_lbl)
        feed_information_layout.addWidget(self.fc_signal)
        feed_information_layout.insertSpacing(-1, spacing)
        feed_information_layout.addWidget(nn_signal_lbl)
        feed_information_layout.addWidget(self.nn_signal)
        feed_information_layout.insertSpacing(-1, spacing)
        feed_information_layout.addWidget(feed_mod_lbl)
        feed_information_layout.addWidget(self.feed_mod)
        feed_information_layout.insertSpacing(-1, spacing)
        feed_information_layout.addWidget(feed_frac_lbl)
        feed_information_layout.addWidget(self.feed_frac)
        feed_information_layout.insertSpacing(-1, 60)
        self.vid_player = VideoPlayer()
        self.video_thread = QThread()
        self.vid_player.moveToThread(self.video_thread)
        self.video_thread.started.connect(self.vid_player.run)
        self.vid_player.finished.connect(self.video_thread.quit)
        self.vid_player.finished.connect(self.vid_player.deleteLater)
        self.video_thread.finished.connect(self.video_thread.deleteLater)
        self.vid_player.vid_px_map.connect(self.update_display)
        # Step 5: Connect signals and slots
        # self.video_thread.started.connect(self.worker.run)
        # self.video_thread.start()

        self.results_layout.addLayout(tray_information_layout)
        self.results_layout.addLayout(feed_information_layout)
        self.im_display = QLabel()
        self.addLayout(display_information)
        self.addWidget(self.im_display)
        self.addLayout(self.results_layout)

    def update_display(self, pixmap):
        self.im_display.setPixmap(pixmap)

    def display_tray_images(self, im, im_message = ''):
        im_date = im.split(SLASH)[-1][:19]
        im_date = im_date.replace('-', ' ')
        self.vid_date_display.setText(im_date)
        self.vid_player.im = im
        self.vid_player.im_message = im_message
        self.video_thread.start()

    def display_tray_info(self, line_data):
        if 'Feed segment avg' in line_data:
            fc_signal = line_data['fc signal']
            class_signal = line_data['classifier signal']
            feed_seg = line_data['Feed segment avg']
        else:
            fc_signal = line_data['fc_signal']
            class_signal = line_data['classifier_signal']
            feed_seg = line_data['Feed_segment_avg']

        self.age = line_data['tray_age'] if 'tray_age' in line_data else -1
        feed_pumped = line_data['food_pumped'] if 'food_pumped' in line_data else -1
        feed_mod = line_data['feed_modifier'] if 'feed_modifier' in line_data else -1

        self.fc_signal.display(fc_signal)
        self.nn_signal.display(class_signal)
        self.feed_frac.display(feed_seg)
        self.feed_pumped.display(feed_pumped)
        self.tray_age.display(self.age)
        self.feed_mod.display(feed_mod)

    def reset_displays(self):
        reset_nr = -1
        self.fc_signal.display(reset_nr)
        self.nn_signal.display(reset_nr)
        self.feed_frac.display(reset_nr)
        self.feed_mod.display(reset_nr)
        self.feed_pumped.display(reset_nr)
        self.tray_age.display(reset_nr)
        self.cumm_food.display(reset_nr)

    def reset_video(self):
        self.im = ''


class BrowseRecordings(BaseBrowser):


    def __init__(self, *args, **kwargs):
        super(BrowseRecordings, self).__init__('Browse recordings', *args, **kwargs)
        self.data_format = 'avi'
        self.image_sub_folder = ''
        self.im_path = expanduser('~/Data/woodfarm/10-6')
        self.file_path.setText(self.im_path)
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addLayout(self.file_layout)
        main_layout.addLayout(self.layout)

        main_layout.addLayout(self.information_layout)
        main_layout.addLayout(self.cntrl_layout)

        widget = QWidget()
        widget.setLayout(main_layout)
        self.setCentralWidget(widget)

    def load_image(self):
        im = self.images_to_browse[self.counter]
        if im[-3:] == 'avi':
            cap = cv2.VideoCapture(im)
            ret, frame = cap.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            height, width, channel = frame.shape
            bytesPerLine = 3 * width
            q_img = QImage(frame.data, width, height, bytesPerLine, QImage.Format_RGB888)
            pixmap = QPixmap(q_img)
        else:
            pixmap = QPixmap(im)
        self.file_path_shown.setText(im)
        # im = join(self.im_path, im)

        pixmap = pixmap.scaled(pixmap.width() / 3, pixmap.height() / 3)
        # self.resize()
        self.im_display.setPixmap(pixmap)
        self.im_displayed = im

    def load_prev(self):
        if self.counter == 0:
            self.counter = len(self.images_to_browse)
            # return

        self.counter -= 1

        self.load_image()

    def load_next(self):
        if self.counter == len(self.images_to_browse) - 1:
            self.counter = -1
        self.counter += 1
        self.load_image()

    def get_image_list(self, image_sub_folder='', data_format='png'):
        """Create a list of files in a folder that have a certain
        format appended to their name (can be anything)."""
        file_path = self.im_path
        if not isdir(file_path):
            Exception("{} is not a direcory".format(file_path))

        if len(image_sub_folder) > 1:
            folder_list = [join(file_path, d, f) for d in listdir(file_path) for f in
                           listdir(join(file_path, d)) if isdir(join(file_path, d, f))]
            file_list = [join(d, f) for d in folder_list for f in listdir(d) if
                         f[-3:] == data_format]
        else:
            file_list = [join(file_path, d) for d in listdir(file_path)]
        self.images_to_browse = file_list


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = BrowseRecordings()
    window.get_image_list()
    window.load_image()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
