import pathlib
import random
import sys
import time
from argparse import ArgumentParser
from datetime import datetime
from enum import Enum
from os import listdir, makedirs, name
from os.path import expanduser, join, isdir, isfile
from shutil import copyfile
from threading import Thread

import cv2
import numpy as np
import pandas as pd
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QLabel,QMessageBox,QMessageBox,QWidget
from tools.general.helpers import timestamp_from_data_time

from browse_recordings import BaseBrowser, FeedSessDisplay, SLASH


class Direction(Enum):
    up = 1
    down = 2


class CheckPerformance(BaseBrowser):
    trays_seen = 0
    in_tray_counter = 0
    len_data_to_browse = 0
    global currentYear, currentMonth
    last_t_stamp = -1
    currentMonth = datetime.now().month
    currentYear = datetime.now().year
    date_key = "%Y-%m-%d_%H_%M_%S"
    image_save_folder = expanduser('~/save_checked_images')
    image_shown_name = ''
    store_vids = False
    keep_playing = True
    max_t_diff = 3
    browse_direction = Direction.up
    vid_is_playing = False

    def __init__(self, date_sub, im_path, store_vids, *args, **kwargs):
        super(CheckPerformance, self).__init__('Results', *args, **kwargs)
        self.vid_t_stamps = np.array([])
        self.im_path = expanduser(im_path)
        self.screen_save_path = expanduser('~/Data/result_scrn_caps')
        self.store_vids = store_vids
        if not isdir(self.screen_save_path):
            makedirs(self.screen_save_path)
        if not isdir(self.image_save_folder):
            makedirs(self.image_save_folder)
        full_date = '20' + date_sub + '_00_00'
        self.start_date = timestamp_from_data_time(full_date, self.date_key)
        # print("full_date", full_date, self.start_date, self.date_key)
        self.file_path.setText(self.im_path)
        self.curr_feed_sess = FeedSessDisplay('Tray state', 0.5)
        self.df_tray = pd.DataFrame()

    def load_layout(self):
        self.save_image_button.clicked.connect(self.save_image)
        self.cntrl_layout.addWidget(self.save_image_button, 0)
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addLayout(self.file_layout)
        display_layout = QtWidgets.QHBoxLayout()
        seg_result_layout = QtWidgets.QVBoxLayout()
        display_layout.addLayout(self.curr_feed_sess)
        self.seg_result_disp = QLabel()
        self.seg_after_feed_disp = QLabel()
        seg_result_layout.addWidget(self.seg_result_disp)
        seg_result_layout.addWidget(self.seg_after_feed_disp)
        display_layout.addLayout(seg_result_layout)
        main_layout.addLayout(display_layout)
        # main_layout.addLayout(self.results_layout)
        main_layout.addLayout(self.information_layout)
        main_layout.addLayout(self.cntrl_layout)

        self.widget = QWidget()
        self.widget.setLayout(main_layout)
        self.setCentralWidget(self.widget)
        self.load_results()
        self.load_next()

    def counter_up(self):
        if self.counter == self.len_data_to_browse - 1:
            self.show_message_done()
            self.set_trays_seen()
            self.trays_seen = 0
            self.counter = -1
        self.counter += 1

    def counter_down(self):
        if self.counter == -1:
            self.counter = self.len_data_to_browse - 1
        self.counter -= 1

    def set_trays_seen(self):
        pass

    def switch_tray(self, switch_dir):
        self.df_tray = None

        for _ in range(8120):
            if switch_dir == 'up':
                self.counter_up()
            else:
                self.counter_down()

            base_folder = self.base_folders[self.counter]
            got_new_tray = self.get_trays_analyses_folder(base_folder)

            if not got_new_tray:

                continue

            # print("checking folder tray ", self.counter, " of ", self.len_data_to_browse)
            if not isfile(self.tray['meas_csv']):
                print("no measurement file found")
                continue
            try:
                df = pd.read_csv(self.tray['meas_csv'])
            except Exception as e:
                print("failed to read measurement file")
                print(e)
                continue
            # print('date new tray:',df['Time and date'])
            self.df_t_stamps = self.create_t_stamps_list(df['Time and date'], '%Y-%m-%d %H:%M:%S')
            first_entry, t_diff = self.get_closest_file_t_stamp(self.start_date, self.df_t_stamps)
            # if t_diff
            if first_entry > -1:
                self.df_tray = df
                is_duplicate = self.df_tray['Time and date'].duplicated()
                self.df_tray = self.df_tray.assign(duplicats=is_duplicate)
                break

        if switch_dir == 'up':
            self.trays_seen += 1
        else:
            self.trays_seen -= 1
            if self.trays_seen < 0:
                self.trays_seen = 0

        path_parts = self.tray['meas_csv'].split(SLASH)[:-1]

        if name == 'posix':
            base_path = join(*path_parts)
            base_path = '/' + base_path
        else:
            base_path = join(*path_parts[1:])
            base_path = path_parts[0] + '\\' + base_path

        recordings_path = join(base_path, 'recordings')
        self.recorded_ims = [join(recordings_path, d) for d in listdir(recordings_path) if
                             d[-3:] in ['avi', 'mkv', 'mk,']]
        recordings_date_times = [d.split(SLASH)[-1][:19] for d in self.recorded_ims]
        self.recordings_t_stamps = self.create_t_stamps_list(recordings_date_times,
                                                             '%Y-%m-%d_%H_%M_%S')
        # print('self.recorded_ims ', self.recorded_ims)
        # print("dates ", self.df_tray['Time and date'])
        seg_results_path = join(base_path, 'feedmap')
        self.seg_results = [join(seg_results_path, d) for d in listdir(seg_results_path) if
                            d[-16:] == 'larv_overlay.png']
        self_seg_t_stamps = [d.split(SLASH)[-1][:19] for d in self.seg_results]

        self.self_seg_t_stamp_list = self.create_t_stamps_list(self_seg_t_stamps,
                                                               '%Y-%m-%d_%H_%M_%S')

        # print("self.seg_results", len(self.seg_results))
        if self.df_tray is None:
            print("could not find file for that date, please set earlier", self.start_date)
            raise Exception

    def print_result(self, title):
        scrn_shot = self.widget.grab()
        scrn_title = title + '.jpg'
        scrn_save_path = join(self.screen_save_path, scrn_title)
        scrn_shot.save(scrn_save_path, 'jpg')

    def load_prev(self):
        self.keep_playing = False
        for _ in range(10000):
            if self.in_tray_counter >= 1:
                self.in_tray_counter -= 1
            else:
                self.switch_tray('down')
                self.in_tray_counter = self.df_tray.shape[0] - 1
            self.browse_direction = Direction.down
            if self.load_image():
                break

    def show_message_done(self):
        msg = QMessageBox()
        msg.setWindowTitle("Jeeej!!")
        msg.setText("You've seen all {} trays".format(self.trays_seen))
        x = msg.exec_()  # this will show our messagebox

    def load_next(self):

        self.keep_playing = False
        for _ in range(10000):

            if self.in_tray_counter < self.df_tray.shape[0] - 1:
                self.in_tray_counter += 1
            else:
                self.switch_tray('up')
                self.in_tray_counter = 0
            self.browse_direction = Direction.up
            if self.load_image():
                break

    def create_t_stamps_list(self, date_time_list, date_key):
        vid_t_stamps = []
        for f in date_time_list:
            file_name = f.split(SLASH)[-1]
            file_date = file_name[:19]
            #           print(file_date)
            t_stamp = timestamp_from_data_time(file_date, date_key)
            vid_t_stamps.append(t_stamp)
        return np.array(vid_t_stamps)

    def get_closest_file_t_stamp(self, ref_t_stamp, t_stamps):

        if t_stamps.shape[0] < 1:
            return -1, 9999

        entry_t_stamp = np.argmin(np.abs(t_stamps - ref_t_stamp))
        closest_entry = entry_t_stamp
        # if closest_entry >= len(t_stamps):
        #     print('overreaching')
        if t_stamps.shape[0] <= closest_entry:
            closest_entry = t_stamps.shape[0] - 1
        t_difference = abs(ref_t_stamp - t_stamps[closest_entry])

        if t_stamps.shape[0] == 1:
            return closest_entry, t_difference

        # print(ref_t_stamp, 'ref entry:', t_stamps[entry_t_stamp + skip_entries], 'difference',t_difference)

        return closest_entry, t_difference

    def save_image(self):
        if len(self.image_shown_name) < 2:
            return

        name_split = self.image_shown_name.split(SLASH)
        tray_name = name_split[-3][-5:-2]
        file_name = name_split[-1]
        save_name = tray_name + '_' + file_name
        full_save_path = join(self.image_save_folder, save_name)
        copyfile(self.image_shown_name, full_save_path)

        if self.store_vids:
            vide_file_path = name_split
            vide_file_path[-2] = 'recordings'
            video_format = 'mkv'
            vide_file_path[-1] = vide_file_path[-1][:-3] + video_format
            full_video_path = join(*vide_file_path)
            if name == 'posix':
                full_video_path = '/' + full_video_path
            # print('saved video', full_video_path)
            video_save_name = save_name[:-3] + video_format
            video_target_save_path = join(self.image_save_folder, video_save_name)
            copyfile(full_video_path, video_target_save_path)

        # print("saved image to", full_save_path)
        self.load_next()

    def display_tray_images(self, line_data):
        self.keep_playing = False
        t_stamp = timestamp_from_data_time(line_data['Time and date'], '%Y-%m-%d %H:%M:%S')
        self.time_stamp = t_stamp
        # print('data ', line_data.dropna)
        overlay_entry, t_diff = self.get_closest_file_t_stamp(t_stamp, self.seg_results,
                                                              "%Y-%m-%d_%H_%M_%S")

        im_entry, t_diff = self.get_closest_file_t_stamp(t_stamp, self.recorded_ims,
                                                         "%Y-%m-%d_%H_%M_%S")
        if t_diff > self.max_t_diff:
            return t_diff

        if im_entry < 0:
            return

        im = self.recorded_ims[im_entry]
        # print("chosen image", im)

        self.video_file = im

        overlay_im = self.seg_results[overlay_entry]
        self.tray_nr.setText(self.tray['nr'])
        self.image_shown_name = im

        if im_entry < 0:
            self.file_path_shown.setText(
                "no corresponding file found {} tray nr: {}".format(line_data['Time and date'],
                                                                    self.tray['nr']))
        else:
            self.file_path_shown.setText(im)

        overlay_im = QPixmap(overlay_im)
        self.overlay_display.setPixmap(overlay_im)

        self.im_displayed = im
        self.last_t_stamp = t_stamp
        # t_stamp = timestamp_from_data_time(time_date)
        if im[-3:] in ['avi', 'mkv', 'mk,']:

            cap = cv2.VideoCapture(im)

            ret, frame = cap.read()
            pixmap = self.gen_qpix_from_frame(frame)
            self.keep_playing = False
            while self.vid_is_playing:
                time.sleep(0.12)
            self.vid_thread = Thread(target=self.run_video, args=(im,))
            self.vid_thread.start()
            # self.vid_thread.
            # vid_thread.join()
        else:
            pixmap = QPixmap(im)
        pixmap = pixmap.scaled(pixmap.width() / 3, pixmap.height() / 3)
        self.im_display.setPixmap(pixmap)

        return 0

    def display_tray_info(self, line_data):
        if 'Feed segment avg' in line_data:
            fc_signal = line_data['fc signal']
            class_signal = line_data['classifier signal']
            feed_seg = line_data['Feed segment avg']
        else:
            fc_signal = line_data['fc_signal']
            class_signal = line_data['classifier_signal']
            feed_seg = line_data['Feed_segment_avg']

        self.age = line_data['tray_age'] if 'tray_age' in line_data else -1
        feed_pumped = line_data['food_pumped'] if 'food_pumped' in line_data else -1
        feed_mod = line_data['feed_modifier'] if 'feed_modifier' in line_data else -1
        self.class_signal = class_signal
        self.fc_signal.display(fc_signal)
        self.nn_signal.display(class_signal)
        self.feed_frac.display(feed_seg)
        self.feed_pumped.display(feed_pumped)
        self.tray_age.display(self.age)
        self.feed_mod.display(feed_mod)

    def load_image(self):
        # print("lookin in folder ",self.counter, 'of', len(self.base_folders))
        line_data = self.df_tray.iloc[self.in_tray_counter]
        # self.disp_cummulative_feed()

        t_stamp = timestamp_from_data_time(line_data['Time and date'], '%Y-%m-%d %H:%M:%S')
        self.time_stamp = t_stamp
        self.last_t_stamp = t_stamp
        im_entry, t_diff = self.get_closest_file_t_stamp(t_stamp, self.recordings_t_stamps)
        t_stamp_vid_next = 0
        if im_entry < 0 or t_diff > self.max_t_diff or t_stamp < self.start_date:
            print("no file found", t_diff, im_entry)
            self.file_path_shown.setText("no corresponding file found {} tray nr: {}".
                                         format(line_data['Time and date'], self.tray['nr']))

            return False
        seg_im_entry, t_diff = self.get_closest_file_t_stamp(t_stamp, self.self_seg_t_stamp_list)
        seg_im = cv2.imread(self.seg_results[seg_im_entry])
        if seg_im.shape[0] < 1000:
            seg_im_qpix = self.gen_qpix_from_frame(seg_im, new_scale=0.8)
            self.seg_result_disp.setPixmap(seg_im_qpix)
        if seg_im_entry + 1 < len(self.seg_results):
            seg_im = cv2.imread(self.seg_results[seg_im_entry + 1])
            if seg_im.shape[0] < 1000:
                seg_im_qpix = self.gen_qpix_from_frame(seg_im, new_scale=0.8)
                self.seg_after_feed_disp.setPixmap(seg_im_qpix)

        im = self.recorded_ims[im_entry]
        im_name_split = im.split(SLASH)
        t_stamp_vid = timestamp_from_data_time(im_name_split[-1][:19], '%Y-%m-%d_%H_%M_%S')
        self.file_path_shown.setText(im)
        # self.tray_nr.setText(self.tray['nr'])
        #
        self.curr_feed_sess.display_tray_images(im)
        self.curr_feed_sess.display_tray_info(line_data)

    def gen_qpix_from_frame(self, frame, new_scale=0.6):

        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (0, 0), fx=new_scale, fy=new_scale)
        height, width, channel = frame.shape
        bytesPerLine = 3 * width
        q_img = QImage(frame.data, width, height, bytesPerLine, QImage.Format_RGB888)
        return QPixmap(q_img)

    def closeEvent(self, event):
        self.keep_playing = False

    def get_trays_analyses_folder(self, base_folder):

        base_folder = join(self.im_path, base_folder)
        recorded_ims = []
        seg_results = []
        feed_maps = join(base_folder, 'feedmap')
        recordings = join(base_folder, 'recordings')

        if isdir(feed_maps) and isdir(recordings):
            tray_nr = base_folder.split(SLASH)[-1].replace('container_box', '').split('_')[0]

            if not tray_nr.isnumeric() or int(
                    tray_nr) > 667:  # 666 is the unidentified tray nr. Anything larger was for testing purposes
                return False

            meas_csv = [join(base_folder, d) for d in listdir(base_folder) if d[-16:] == 'measurements.csv']
            # tray_nr = base_folder[-5:-2]

            if len(meas_csv):
                # recording_list = listdir(recordings)

                fname = pathlib.Path(join(base_folder, 'measurements.csv'))
                last_modified = fname.stat().st_mtime
                if last_modified < self.start_date:
                    return False
                # tray_nr= tray_nr.replace('x','').replace('o','').replace('/','')
                new_tray = dict({'nr': tray_nr, 'recorded_ims': recorded_ims,
                                 'seg_results': seg_results, 'meas_csv': meas_csv[0]})

                self.tray = new_tray
                self.data_to_browse.append(new_tray)
                return True
        print('not dir feedmap',isdir(feed_maps))
        return False

    def load_results(self):
        self.base_folders = listdir(self.im_path)
        random.shuffle(self.base_folders)
        self.data_to_browse = []

        for base_folder in self.base_folders[0:]:
            got_tray = self.get_trays_analyses_folder(base_folder)
            if got_tray:
                break

        # self.tray = self.data_to_browse[0]
        self.len_data_to_browse = len(self.base_folders)
        print("found ", self.len_data_to_browse, "trays")


if __name__ == '__main__':
    parser = ArgumentParser()

    app = QtWidgets.QApplication(sys.argv)
    parser.add_argument('-t', '--date_time', help="Set day month and hour as follows: YY-MM-DD_hh",
                        type=str, required=False, default='21-08-10_12')
    parser.add_argument('-p', '--path', help="path to files",
                        type=str, required=False, default='/media/x1-2/SWAPDISK')
    parser.add_argument('-v', '--store_vids', help="Also store the videos",
                        type=bool, required=False, default=True)
    parser.add_argument('-c', '--configs_path', help="Path to folder where the config files are "
                                                     "located for the individual containers",
                        type=str, required=False, default='~/TrayInspector/config_files')

    args = parser.parse_args(sys.argv[1:])
    window = CheckPerformance(args.date_time, args.path, args.store_vids)
    window.load_layout()
    window.show()
    app.exec_()
