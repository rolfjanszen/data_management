from os.path import expanduser, join, isdir
from os import mkdir

import cv2
import argparse
import sys
tool_box_path = expanduser('~/scripts/tools')
sys.path.append(tool_box_path)

from helpers import get_title, get_file_list


def change_size_files(file_path, files, save_append, new_scale):
    full_save_path = join(file_path, save_append)
    if not isdir(full_save_path):
        mkdir(full_save_path)

    for file in files:
        im = cv2.imread(join(file_path, file))
        im = cv2.resize(im, (0, 0), fx=new_scale, fy=new_scale)
        new_name = join(full_save_path, file)
        new_name.replace(':','_')
        print('created new file: ', new_name)
        cv2.imwrite(new_name, im)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--file_path', help="File path to the files to be concatenaed into a video",
                        type=str, required=True)
    parser.add_argument('--file_type', type=str, required=False,
                        default='png', help='type of (last 3 letters) input files, e.g. avi, jpg, png... can be a longer name.'
                        'I.e. segm_rslt.png to get certain images from a file.')
    parser.add_argument('--save_append', help="Add string for path to save files",
                        type=str, required=False, default='')
    parser.add_argument('--scale', help="New scale of images, default is 0.5",
                        type=float, required=False, default=0.5)

    args = parser.parse_args()
    file_path = expanduser(args.file_path)
    files = get_file_list(file_path, args.file_type)
    change_size_files(file_path, files, args.save_append, args.scale)
